plugins {
    kotlin("jvm") version "2.0.21"
    application
}

group = "fi.nylund"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))
    implementation(kotlin("stdlib"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.9.0")
    implementation("com.varabyte.kotter:kotter-jvm:1.2.0")
    implementation("io.ktor:ktor-client-core:3.0.1")
    implementation("io.ktor:ktor-client-cio:3.0.1")
    implementation("io.ktor:ktor-client-content-negotiation:3.0.1")
    implementation(libs.kotlinx.serialization)

}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(21)
}

application {
    applicationDefaultJvmArgs = listOf("-XX:+UseZGC")
    mainClass.set("Main2024Kt")
}