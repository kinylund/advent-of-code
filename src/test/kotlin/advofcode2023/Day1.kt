package advofcode2023

import kotlin.test.Test
import kotlin.test.assertEquals

class Day1Test {

    @Test
    fun testDay1a() {
        val testString = "1abc2\n" +
                "pqr3stu8vwx\n" +
                "a1b2c3d4e5f\n" +
                "treb7uchet"
        assertEquals(142, day1a(testString))
    }

    @Test
    fun testDay1b() {
        val testString = "two1nine\n" +
                "eightwothree\n" +
                "abcone2threexyz\n" +
                "xtwone3four\n" +
                "4nineeightseven2\n" +
                "zoneight234\n" +
                "7pqrstsixteen"
        assertEquals(281, day1b(testString))
    }
}