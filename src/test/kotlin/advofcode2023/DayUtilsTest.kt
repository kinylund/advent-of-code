package advofcode2023

import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class DayUtilsTest {

    @Test
    fun testDay25a() {
        val testString =
            "jqt: rhn xhk nvd\n" +
                    "rsh: frs pzl lsr\n" +
                    "xhk: hfx\n" +
                    "cmg: qnr nvd lhk bvb\n" +
                    "rhn: xhk bvb hfx\n" +
                    "bvb: xhk hfx\n" +
                    "pzl: lsr hfx nvd\n" +
                    "qnr: nvd\n" +
                    "ntq: jqt hfx bvb xhk\n" +
                    "nvd: lhk\n" +
                    "lsr: lhk\n" +
                    "rzs: qnr cmg lsr rsh\n" +
                    "frs: qnr lhk lsr"
        runBlocking {
            assertEquals(54, day25a(testString))
        }
    }

    @Test
    fun testDay24b() {
        // X position 24, Y position 13, and Z position 10.
        val testString =
            "19, 13, 30 @ -2,  1, -2\n" +
                    "18, 19, 22 @ -1, -1, -2\n" +
                    "20, 25, 34 @ -2, -2, -4\n" +
                    "12, 31, 28 @ -1, -2, -1\n" +
                    "20, 19, 15 @  1, -5, -3"
        runBlocking {
            assertEquals(47, day24b(testString, 512L))
        }
    }

    @Test
    fun testDay24a() {
        val testString =
            "19, 13, 30 @ -2,  1, -2\n" +
                    "18, 19, 22 @ -1, -1, -2\n" +
                    "20, 25, 34 @ -2, -2, -4\n" +
                    "12, 31, 28 @ -1, -2, -1\n" +
                    "20, 19, 15 @  1, -5, -3"
        assertEquals(2, day24a(testString, 7.toBigDecimal()..27.toBigDecimal()))
    }

    @Test
    fun testDay23() {
        val testString =
            "#.#####################\n" +
                    "#.......#########...###\n" +
                    "#######.#########.#.###\n" +
                    "###.....#.>.>.###.#.###\n" +
                    "###v#####.#v#.###.#.###\n" +
                    "###.>...#.#.#.....#...#\n" +
                    "###v###.#.#.#########.#\n" +
                    "###...#.#.#.......#...#\n" +
                    "#####.#.#.#######.#.###\n" +
                    "#.....#.#.#.......#...#\n" +
                    "#.#####.#.#.#########v#\n" +
                    "#.#...#...#...###...>.#\n" +
                    "#.#.#v#######v###.###v#\n" +
                    "#...#.>.#...>.>.#.###.#\n" +
                    "#####v#.#.###v#.#.###.#\n" +
                    "#.....#...#...#.#.#...#\n" +
                    "#.#########.###.#.#.###\n" +
                    "#...###...#...#...#.###\n" +
                    "###.###.#.###v#####v###\n" +
                    "#...#...#.#.>.>.#.>.###\n" +
                    "#.###.###.#.###.#.#v###\n" +
                    "#.....###...###...#...#\n" +
                    "#####################.#"
        assertEquals(94, day23(testString, false))
        assertEquals(154, day23(testString, true))
    }

    @Test
    fun testDay22a() {
        val testString =
            "1,0,1~1,2,1\n" +
                    "0,0,2~2,0,2\n" +
                    "0,2,3~2,2,3\n" +
                    "0,0,4~0,2,4\n" +
                    "2,0,5~2,2,5\n" +
                    "0,1,6~2,1,6\n" +
                    "1,1,8~1,1,9"

        runBlocking {
            assertEquals(5, day22a(testString))
        }
    }

    @Test
    fun testDay22b() {
        val testString =
            "1,0,1~1,2,1\n" +
                    "0,0,2~2,0,2\n" +
                    "0,2,3~2,2,3\n" +
                    "0,0,4~0,2,4\n" +
                    "2,0,5~2,2,5\n" +
                    "0,1,6~2,1,6\n" +
                    "1,1,8~1,1,9"

        runBlocking {
            assertEquals(7, day22b(testString))
        }
    }

    @Test
    fun testDay21b() {
        val testString =
            "...........\n" +
                    ".....###.#.\n" +
                    ".###.##..#.\n" +
                    "..#.#...#..\n" +
                    "....#.#....\n" +
                    ".##..S####.\n" +
                    ".##..#...#.\n" +
                    ".......##..\n" +
                    ".##.#.####.\n" +
                    ".##..##.##.\n" +
                    "..........."
        assertEquals(13, day21(testString, 5))
        assertEquals(50, day21(testString, 10))
        assertEquals(115, day21(testString, 15))
        //assertEquals(1594, day21(testString, 50))
        //assertEquals(6536, day21(testString, 100))
        //assertEquals(167004, day21(testString, 500))
        //assertEquals(668697, day21(testString, 1000))
        //assertEquals(16733044, day21(testString, 5000))
    }

    @Test
    fun testDay21a() {
        val testString =
            "...........\n" +
                    ".....###.#.\n" +
                    ".###.##..#.\n" +
                    "..#.#...#..\n" +
                    "....#.#....\n" +
                    ".##..S####.\n" +
                    ".##..#...#.\n" +
                    ".......##..\n" +
                    ".##.#.####.\n" +
                    ".##..##.##.\n" +
                    "..........."
        assertEquals(16, day21a(testString, 6))
    }

    @Test
    fun testDay20a2() {
        val testString =
            "broadcaster -> a, b, c\n" +
                    "%a -> b\n" +
                    "%b -> c\n" +
                    "%c -> inv\n" +
                    "&inv -> a"
        assertEquals(32000000, day20a(testString))
    }

    @Test
    fun testDay20a() {
        val testString =
            "broadcaster -> a\n" +
                    "%a -> inv, con\n" +
                    "&inv -> b\n" +
                    "%b -> con\n" +
                    "&con -> output"
        assertEquals(11687500, day20a(testString))
    }

    @Test
    fun testDay19b() {
        val testString =
            "px{a<2006:qkq,m>2090:A,rfg}\n" +
                    "pv{a>1716:R,A}\n" +
                    "lnx{m>1548:A,A}\n" +
                    "rfg{s<537:gd,x>2440:R,A}\n" +
                    "qs{s>3448:A,lnx}\n" +
                    "qkq{x<1416:A,crn}\n" +
                    "crn{x>2662:A,R}\n" +
                    "in{s<1351:px,qqz}\n" +
                    "qqz{s>2770:qs,m<1801:hdj,R}\n" +
                    "gd{a>3333:R,R}\n" +
                    "hdj{m>838:A,pv}\n" +
                    "\n" +
                    "{x=787,m=2655,a=1222,s=2876}\n" +
                    "{x=1679,m=44,a=2067,s=496}\n" +
                    "{x=2036,m=264,a=79,s=2244}\n" +
                    "{x=2461,m=1339,a=466,s=291}\n" +
                    "{x=2127,m=1623,a=2188,s=1013}"
        assertEquals(167409079868000L, day19b(testString))
    }

    @Test
    fun testDay19a() {
        val testString =
            "px{a<2006:qkq,m>2090:A,rfg}\n" +
                    "pv{a>1716:R,A}\n" +
                    "lnx{m>1548:A,A}\n" +
                    "rfg{s<537:gd,x>2440:R,A}\n" +
                    "qs{s>3448:A,lnx}\n" +
                    "qkq{x<1416:A,crn}\n" +
                    "crn{x>2662:A,R}\n" +
                    "in{s<1351:px,qqz}\n" +
                    "qqz{s>2770:qs,m<1801:hdj,R}\n" +
                    "gd{a>3333:R,R}\n" +
                    "hdj{m>838:A,pv}\n" +
                    "\n" +
                    "{x=787,m=2655,a=1222,s=2876}\n" +
                    "{x=1679,m=44,a=2067,s=496}\n" +
                    "{x=2036,m=264,a=79,s=2244}\n" +
                    "{x=2461,m=1339,a=466,s=291}\n" +
                    "{x=2127,m=1623,a=2188,s=1013}"
        assertEquals(19114, day19a(testString))
    }

    @Test
    fun testDay18b() {
        val testString =
            "R 6 (#70c710)\n" +
                    "D 5 (#0dc571)\n" +
                    "L 2 (#5713f0)\n" +
                    "D 2 (#d2c081)\n" +
                    "R 2 (#59c680)\n" +
                    "D 2 (#411b91)\n" +
                    "L 5 (#8ceee2)\n" +
                    "U 2 (#caa173)\n" +
                    "L 1 (#1b58a2)\n" +
                    "U 2 (#caa171)\n" +
                    "R 2 (#7807d2)\n" +
                    "U 3 (#a77fa3)\n" +
                    "L 2 (#015232)\n" +
                    "U 2 (#7a21e3)"
        assertEquals(952408144115, day18b(testString))
    }

    @Test
    fun testDay18a() {
        val testString =
            "R 6 (#70c710)\n" +
                    "D 5 (#0dc571)\n" +
                    "L 2 (#5713f0)\n" +
                    "D 2 (#d2c081)\n" +
                    "R 2 (#59c680)\n" +
                    "D 2 (#411b91)\n" +
                    "L 5 (#8ceee2)\n" +
                    "U 2 (#caa173)\n" +
                    "L 1 (#1b58a2)\n" +
                    "U 2 (#caa171)\n" +
                    "R 2 (#7807d2)\n" +
                    "U 3 (#a77fa3)\n" +
                    "L 2 (#015232)\n" +
                    "U 2 (#7a21e3)"
        assertEquals(62, day18a(testString))
    }

    @Test
    fun testDay17b2() {
        val testString =
            "111111111111\n" +
                    "999999999991\n" +
                    "999999999991\n" +
                    "999999999991\n" +
                    "999999999991"
        assertEquals(71, day17(testString, true))
    }

    @Test
    fun testDay17b() {
        val testString =
            "2413432311323\n" +
                    "3215453535623\n" +
                    "3255245654254\n" +
                    "3446585845452\n" +
                    "4546657867536\n" +
                    "1438598798454\n" +
                    "4457876987766\n" +
                    "3637877979653\n" +
                    "4654967986887\n" +
                    "4564679986453\n" +
                    "1224686865563\n" +
                    "2546548887735\n" +
                    "4322674655533"
        assertEquals(94, day17(testString, true))
    }

    @Test
    fun testDay17a() {
        val testString =
            "2413432311323\n" +
                    "3215453535623\n" +
                    "3255245654254\n" +
                    "3446585845452\n" +
                    "4546657867536\n" +
                    "1438598798454\n" +
                    "4457876987766\n" +
                    "3637877979653\n" +
                    "4654967986887\n" +
                    "4564679986453\n" +
                    "1224686865563\n" +
                    "2546548887735\n" +
                    "4322674655533"
        assertEquals(102, day17(testString, false))
    }

    @Test
    fun testDay16a() {
        val testString =
            ".|...\\....\n" +
                    "|.-.\\.....\n" +
                    ".....|-...\n" +
                    "........|.\n" +
                    "..........\n" +
                    ".........\\\n" +
                    "..../.\\\\..\n" +
                    ".-.-/..|..\n" +
                    ".|....-|.\\\n" +
                    "..//.|...."
        assertEquals(46, day16a(testString))
    }

    @Test
    fun testDay15b() {
        val testString =
            "rn=1," +
                    "cm-," +
                    "qp=3," +
                    "cm=2," +
                    "qp-," +
                    "pc=4," +
                    "ot=9," +
                    "ab=5," +
                    "pc-," +
                    "pc=6," +
                    "ot=7"
        assertEquals(145, day15b(testString))
    }

    @Test
    fun testDay15() {
        val testString = "HASH"

        assertEquals(52, day15a(testString))
    }

    @Test
    fun testDay14b() {
        val testString =
            "O....#....\n" +
                    "O.OO#....#\n" +
                    ".....##...\n" +
                    "OO.#O....O\n" +
                    ".O.....O#.\n" +
                    "O.#..O.#.#\n" +
                    "..O..#O..O\n" +
                    ".......O..\n" +
                    "#....###..\n" +
                    "#OO..#...."

        assertEquals(64, day14b(testString, 500))
    }

    @Test
    fun testDay14a() {
        val testString =
            "O....#....\n" +
                    "O.OO#....#\n" +
                    ".....##...\n" +
                    "OO.#O....O\n" +
                    ".O.....O#.\n" +
                    "O.#..O.#.#\n" +
                    "..O..#O..O\n" +
                    ".......O..\n" +
                    "#....###..\n" +
                    "#OO..#...."

        assertEquals(136, day14a(testString))
    }

    @Test
    fun testDay13b2() {
        val testString =
            "#..##.#\n" +
                    ".##..##\n" +
                    "#..###.\n" +
                    ".....##\n" +
                    ".##..##\n" +
                    ".##..#.\n" +
                    ".....##"

        assertEquals(500, day13(testString, false))
    }

    @Test
    fun testDay13a2() {
        val testString =
            "#..##.#\n" +
                    ".##..##\n" +
                    "#..###.\n" +
                    ".....##\n" +
                    ".##..##\n" +
                    ".##..#.\n" +
                    ".....##"

        assertEquals(2, day13(testString, true))
    }

    @Test
    fun testDay13a() {
        val testString =
            "#.##..##.\n" +
                    "..#.##.#.\n" +
                    "##......#\n" +
                    "##......#\n" +
                    "..#.##.#.\n" +
                    "..##..##.\n" +
                    "#.#.##.#.\n" +
                    "\n" +
                    "#...##..#\n" +
                    "#....#..#\n" +
                    "..##..###\n" +
                    "#####.##.\n" +
                    "#####.##.\n" +
                    "..##..###\n" +
                    "#....#..#"

        assertEquals(405, day13(testString, true))
    }

    @Test
    fun testDay13b() {
        val testString =
            "#.##..##.\n" +
                    "..#.##.#.\n" +
                    "##......#\n" +
                    "##......#\n" +
                    "..#.##.#.\n" +
                    "..##..##.\n" +
                    "#.#.##.#.\n" +
                    "\n" +
                    "#...##..#\n" +
                    "#....#..#\n" +
                    "..##..###\n" +
                    "#####.##.\n" +
                    "#####.##.\n" +
                    "..##..###\n" +
                    "#....#..#"

        assertEquals(400, day13(testString, false))
    }

    @Test
    fun testDay12b() {
        val testString = "???.### 1,1,3\n" +
                ".??..??...?##. 1,1,3\n" +
                "?#?#?#?#?#?#?#? 1,3,1,6\n" +
                "????.#...#... 4,1,1\n" +
                "????.######..#####. 1,6,5\n" +
                "?###???????? 3,2,1"
        runBlocking {
            assertEquals(206, day12b(testString, 2))
            assertEquals(2612, day12b(testString, 3))
            assertEquals(36308, day12b(testString, 4))
            assertEquals(525152, day12b(testString, 5))
            assertEquals(7737356, day12b(testString, 6))
        }
    }

    @Test
    fun testDay12a() {
        val testString = "???.### 1,1,3\n" +
                ".??..??...?##. 1,1,3\n" +
                "?#?#?#?#?#?#?#? 1,3,1,6\n" +
                "????.#...#... 4,1,1\n" +
                "????.######..#####. 1,6,5\n" +
                "?###???????? 3,2,1"

        assertEquals(21, day12(testString, 1))
    }

    @Test
    fun testDay12av2() {
        //val testString = "???.### 1,1,3"
        val testString = "???.### 1,1,3\n" +
                ".??..??...?##. 1,1,3\n" +
                "?#?#?#?#?#?#?#? 1,3,1,6\n" +
                "????.#...#... 4,1,1\n" +
                "????.######..#####. 1,6,5\n" +
                "?###???????? 3,2,1"
        runBlocking {
            assertEquals(21, day12b(testString, 1))
        }
    }

    @Test
    fun testDay12av3() {
        val testString = "???.### 1,1,3\n" +
                ".??..??...?##. 1,1,3\n" +
                "?#?#?#?#?#?#?#? 1,3,1,6\n" +
                "????.#...#... 4,1,1\n" +
                "????.######..#####. 1,6,5\n" +
                "?###???????? 3,2,1\n" +
                "???.???????? 1,2,1,1\n" +
                ".#.????????##?????? 1,1,1,4,1,1\n" +
                "??????????.???????? 1,1,5,1,1,1\n" +
                "???????##??.??????? 1,1,4,1,2,1\n" +
                "?#??????????????? 1,4,1,1\n" +
                "???????????#??????? 1,3,4,2,1"
        runBlocking {
            assertEquals(35627888, day12b(testString, 3))
        }
    }


    @Test
    fun testDay11b() {
        val testString =
            "...#......\n" +
                    ".......#..\n" +
                    "#.........\n" +
                    "..........\n" +
                    "......#...\n" +
                    ".#........\n" +
                    ".........#\n" +
                    "..........\n" +
                    ".......#..\n" +
                    "#...#....."
        assertEquals(374, day11b(testString, 2))
        assertEquals(1030, day11b(testString, 10))
        assertEquals(8410, day11b(testString, 100))
    }

    @Test
    fun testDay11a() {
        val testString =
            "...#......\n" +
                    ".......#..\n" +
                    "#.........\n" +
                    "..........\n" +
                    "......#...\n" +
                    ".#........\n" +
                    ".........#\n" +
                    "..........\n" +
                    ".......#..\n" +
                    "#...#....."

        assertEquals(374, day11a(testString))
    }

    @Test
    fun testDay10b3() {
        val testString =
            ".F----7F7F7F7F-7....\n" +
                    ".|F--7||||||||FJ....\n" +
                    ".||.FJ||||||||L7....\n" +
                    "FJL7L7LJLJ||LJ.L-7..\n" +
                    "L--J.L7...LJS7F-7L7.\n" +
                    "....F-J..F7FJ|L7L7L7\n" +
                    "....L7.F7||L7|.L7L7|\n" +
                    ".....|FJLJ|FJ|F7|.LJ\n" +
                    "....FJL-7.||.||||...\n" +
                    "....L---J.LJ.LJLJ..."

        assertEquals(8, day10b(testString))
    }

    @Test
    fun testDay10b2() {
        val testString = "FF7FSF7F7F7F7F7F---7\n" +
                "L|LJ||||||||||||F--J\n" +
                "FL-7LJLJ||||||LJL-77\n" +
                "F--JF--7||LJLJ7F7FJ-\n" +
                "L---JF-JLJ.||-FJLJJ7\n" +
                "|F|F-JF---7F7-L7L|7|\n" +
                "|FFJF7L7F-JF7|JL---7\n" +
                "7-L-JL7||F7|L7F-7F7|\n" +
                "L.L7LFJ|||||FJL7||LJ\n" +
                "L7JLJL-JLJLJL--JLJ.L"

        assertEquals(10, day10b(testString))
    }

    @Test
    fun testDay10b() {
        val testString = "..........\n" +
                ".S------7.\n" +
                ".|F----7|.\n" +
                ".||OOOO||.\n" +
                ".||OOOO||.\n" +
                ".|L-7F-J|.\n" +
                ".|II||II|.\n" +
                ".L--JL--J.\n" +
                ".........."

        assertEquals(4, day10b(testString))
    }

    @Test
    fun testDay10a2() {
        val testString = "..F7.\n" +
                ".FJ|.\n" +
                "SJ.L7\n" +
                "|F--J\n" +
                "LJ..."

        assertEquals(8, day10a(testString))
    }

    @Test
    fun testDay10a() {
        val testString = ".....\n" +
                ".S-7.\n" +
                ".|.|.\n" +
                ".L-J.\n" +
                "....."

        assertEquals(4, day10a(testString))

    }

    @Test
    fun testDay9b() {
        val testString = "0 3 6 9 12 15\n" +
                "1 3 6 10 15 21\n" +
                "10 13 16 21 30 45"

        assertEquals(2, day9b(testString))
    }

    @Test
    fun testDay9a() {
        val testString = "0 3 6 9 12 15\n" +
                "1 3 6 10 15 21\n" +
                "10 13 16 21 30 45"

        assertEquals(114, day9a(testString))
    }

    @Test
    fun testDay8b() {
        val testString = "LR\n" +
                "\n" +
                "11A = (11B, XXX)\n" +
                "11B = (XXX, 11Z)\n" +
                "11Z = (11B, XXX)\n" +
                "22A = (22B, XXX)\n" +
                "22B = (22C, 22C)\n" +
                "22C = (22Z, 22Z)\n" +
                "22Z = (22B, 22B)\n" +
                "XXX = (XXX, XXX)"

        assertEquals(6, day8b(testString))
    }

    @Test
    fun testDay8a() {
        val testString = "LLR\n" +
                "\n" +
                "AAA = (BBB, BBB)\n" +
                "BBB = (AAA, ZZZ)\n" +
                "ZZZ = (ZZZ, ZZZ)"

        assertEquals(6, day8a(testString))
    }

    @Test
    fun testDay7a() {
        val testString = "32T3K 765\n" +
                "T55J5 684\n" +
                "KK677 28\n" +
                "KTJJT 220\n" +
                "QQQJA 483"

        assertEquals(6440, day7a(testString))
    }

    @Test
    fun testDay7b() {
        val testString = "32T3K 765\n" +
                "T55J5 684\n" +
                "KK677 28\n" +
                "KTJJT 220\n" +
                "QQQJA 483"

        assertEquals(5905, day7b(testString))
    }

    @Test
    fun testDay6a() {
        val races = listOf(7L to 9L, 15L to 40L, 30L to 200L)
        assertEquals(288, day6(races))
    }

    @Test
    fun testDay5b() {
        val testString = "seeds: 79 14 55 13\n" +
                "\n" +
                "seed-to-soil map:\n" +
                "50 98 2\n" +
                "52 50 48\n" +
                "\n" +
                "soil-to-fertilizer map:\n" +
                "0 15 37\n" +
                "37 52 2\n" +
                "39 0 15\n" +
                "\n" +
                "fertilizer-to-water map:\n" +
                "49 53 8\n" +
                "0 11 42\n" +
                "42 0 7\n" +
                "57 7 4\n" +
                "\n" +
                "water-to-light map:\n" +
                "88 18 7\n" +
                "18 25 70\n" +
                "\n" +
                "light-to-temperature map:\n" +
                "45 77 23\n" +
                "81 45 19\n" +
                "68 64 13\n" +
                "\n" +
                "temperature-to-humidity map:\n" +
                "0 69 1\n" +
                "1 0 69\n" +
                "\n" +
                "humidity-to-location map:\n" +
                "60 56 37\n" +
                "56 93 4"
        assertEquals(46, runBlocking { day5b(testString) })
        assertEquals(46, runBlocking { day5b2(testString) })
    }

    @Test
    fun testDay5a() {
        val testString =
            "seeds: 79 14 55 13\n" +
                    "\n" +
                    "seed-to-soil map:\n" +
                    "50 98 2\n" +
                    "52 50 48\n" +
                    "\n" +
                    "soil-to-fertilizer map:\n" +
                    "0 15 37\n" +
                    "37 52 2\n" +
                    "39 0 15\n" +
                    "\n" +
                    "fertilizer-to-water map:\n" +
                    "49 53 8\n" +
                    "0 11 42\n" +
                    "42 0 7\n" +
                    "57 7 4\n" +
                    "\n" +
                    "water-to-light map:\n" +
                    "88 18 7\n" +
                    "18 25 70\n" +
                    "\n" +
                    "light-to-temperature map:\n" +
                    "45 77 23\n" +
                    "81 45 19\n" +
                    "68 64 13\n" +
                    "\n" +
                    "temperature-to-humidity map:\n" +
                    "0 69 1\n" +
                    "1 0 69\n" +
                    "\n" +
                    "humidity-to-location map:\n" +
                    "60 56 37\n" +
                    "56 93 4"
        assertEquals(35, day5a(testString))
    }

    @Test
    fun testDay4b() {
        val testString =
            "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53\n" +
                    "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19\n" +
                    "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1\n" +
                    "Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83\n" +
                    "Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36\n" +
                    "Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"
        val spaceSplitter = " +".toRegex()
        val dataPairs = testString.split("\n")
            .map { row: String ->
                row.split(": ", " | ")
                    .map { it.trim() }
                    .drop(1)
                    .let { numbers ->
                        val winSet = numbers.last().split(spaceSplitter)
                            .map {
                                it.toInt()
                            }.toSet()
                        val selected = numbers.first().split(spaceSplitter)
                            .map {
                                it.toInt()
                            }.toSet()
                        winSet.intersect(selected).size
                    }
            }
        assertEquals(30, day4b(dataPairs))
    }

    @Test
    fun testDay4a() {
        val testString =
            "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53\n" +
                    "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19\n" +
                    "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1\n" +
                    "Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83\n" +
                    "Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36\n" +
                    "Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"
        assertEquals(13, day4a(testString))
    }

    @Test
    fun testDay3b() {
        val testString =
            "467..114..\n" +
                    "...*......\n" +
                    "..35..633.\n" +
                    "......#...\n" +
                    "617*......\n" +
                    ".....+.58.\n" +
                    "..592.....\n" +
                    "......755.\n" +
                    "...\$.*....\n" +
                    ".664.598.."
        assertEquals(467835, day3b(testString))
    }

    @Test
    fun testDay3a() {
        val testString =
            "467..114..\n" +
                    "...*......\n" +
                    "..35..633.\n" +
                    "......#...\n" +
                    "617*......\n" +
                    ".....+.58.\n" +
                    "..592.....\n" +
                    "......755.\n" +
                    "...\$.*....\n" +
                    ".664.598.."
        assertEquals(4361, day3a(testString))
    }

    @Test
    fun testDay2a() {
        val testString = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green\n" +
                "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue\n" +
                "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red\n" +
                "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red\n" +
                "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"
        assertEquals(8, day2a(testString))
    }

    @Test
    fun testDay2b() {
        val testString = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green\n" +
                "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue\n" +
                "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red\n" +
                "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red\n" +
                "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"
        assertEquals(2286, day2b(testString))
    }

    @Test
    fun testDay1a() {
        val testString = "1abc2\n" +
                "pqr3stu8vwx\n" +
                "a1b2c3d4e5f\n" +
                "treb7uchet"
        assertEquals(142, day1a(testString))
    }

    @Test
    fun testDay1b() {
        val testString = "two1nine\n" +
                "eightwothree\n" +
                "abcone2threexyz\n" +
                "xtwone3four\n" +
                "4nineeightseven2\n" +
                "zoneight234\n" +
                "7pqrstsixteen"
        assertEquals(281, day1b(testString))
    }

}