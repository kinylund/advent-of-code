import org.junit.jupiter.api.Assertions.*
import kotlin.test.Test

class UtilsTest {

    @Test
    fun testStringsToDigit() {
        assertEquals("18", stringsToDigit("oneight"))
        assertEquals("76", stringsToDigit("7pqrstsixteen"))
    }

    @Test
    fun testMergeAscending() {
        val list1 = listOf(0L..3L, 4L..5L, 7L..132L, 8L..56L)
        assertEquals(listOf(0L..5L, 7L..132L), list1.mergeAscending())
    }

    @Test
    fun testPartitionList() {
        val result = (0L..10L).partition(1L..2L)
        assertEquals(1L..2L, result.first)
        assertEquals(listOf(0L..0L, 3L..10L), result.second)
    }

    @Test
    fun testPartitionList2() {
        val result = (0L..10L).partition(100L..210L)
        assertEquals(null, result.first)
        assertEquals(listOf(0L..10L), result.second)
    }

    @Test
    fun testPartitionList4() {
        val result = (6L..10L).partition(0L..8L)
        assertEquals(6L..8L, result.first)
        assertEquals(listOf(9L..10L), result.second)
    }

    @Test
    fun testPartitionList5() {
        val result = (6L..10L).partition(9L..13L)
        assertEquals(9L..10L, result.first)
        assertEquals(listOf(6L..8L), result.second)
    }

    @Test
    fun testPartitionList3() {
        val result = (1000L..100000L).partition(100L..210L)
        assertEquals(null, result.first)
        assertEquals(listOf(1000L..100000L), result.second)
    }

    @Test
    fun testChunkLongRange() {
        val chunked = (0L..10L).chunk(3)
        assertEquals((0L..10L).toList(), chunked.flatten().toList())

        val chunked2 = (0L..10L).chunk(3)
        assertEquals((0L..10L).toList(), chunked2.flatten().toList())
    }

    @Test
    fun testTranspose() {
        val matrix = listOf(listOf(1, 2), listOf(3, 4))
        val transposed = matrix.transpose()
        assertEquals(listOf(listOf(1, 3), listOf(2, 4)), transposed)
    }

    @Test
    fun testRotateRight() {
        val matrix = listOf(listOf(1, 2), listOf(3, 4))
        val transposed = matrix.rotateRight()
        assertEquals(listOf(listOf(3, 1), listOf(4, 2)), transposed)
    }

    @Test
    fun testRotateLeft() {
        val matrix = listOf(listOf(1, 2), listOf(3, 4))
        val transposed = matrix.rotateLeft()
        assertEquals(listOf(listOf(2, 4), listOf(1, 3)), transposed)
    }

    @Test
    fun testListSplitter() {
        val testList = listOf(1, 2, 3, 1, 4, 5, 6, 4)
        assertEquals(listOf(listOf(1), listOf(2, 3), listOf(1), listOf(4, 5, 6, 4)), testList.splitList(1))
    }
}