package advofcode2024

import kotlin.test.Test
import kotlin.test.assertEquals

class Day1Test {

    @Test
    fun testDay1a() {
        val testString = """
       3   4
       4   3
       2   5
       1   3
       3   9
       3   3
        """.trimIndent()
        assertEquals(11, day1a(testString))
    }

    @Test
    fun testDay1b() {
        val testString = """
       3   4
       4   3
       2   5
       1   3
       3   9
       3   3
        """.trimIndent()
        assertEquals(31, day1b(testString))
    }
}