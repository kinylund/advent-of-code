package advofcode2024

import kotlin.test.Test
import kotlin.test.assertEquals

class Day4Test {

    @Test
    fun testDay4a() {
        val testString = """
            MMMSXXMASM
            MSAMXMSMSA
            AMXSXMAAMM
            MSAMASMSMX
            XMASAMXAMM
            XXAMMXXAMA
            SMSMSASXSS
            SAXAMASAAA
            MAMMMXMMMM
            MXMXAXMASX
        """.trimIndent()
        assertEquals(18, day4a(testString))
    }

    @Test
    fun testDay4b() {
        val testString = """
            .M.S......
            ..A..MSMS.
            .M.S.MAA..
            ..A.ASMSM.
            .M.S.M....
            ..........
            S.S.S.S.S.
            .A.A.A.A..
            M.M.M.M.M.
            ..........
        """.trimIndent()
        assertEquals(9, day4b(testString))
    }
}