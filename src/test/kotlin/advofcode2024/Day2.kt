package advofcode2024

import kotlin.test.Test
import kotlin.test.assertEquals

class Day2Test {

    @Test
    fun testDay2a() {
        val testString = """
            7 6 4 2 1
            1 2 7 8 9
            9 7 6 2 1
            1 3 2 4 5
            8 6 4 4 1
            1 3 6 7 9
        """.trimIndent()
        assertEquals(2, day2a(testString))
    }

    @Test
    fun testDay2b() {
        val testString = """
            1 3 6 7 11
            1 5 6 7 9
            7 6 4 2 1
            1 2 7 8 9
            9 7 6 2 1
            1 3 2 4 5
            8 6 4 4 1
            1 3 6 7 9
            1 2 3 8 9
            1 5 6 7 11
            1 3 2 6 9
            1 3 2 6 10
            3 5 6 3 8
            3 2 6 8 9
        """.trimIndent()
        assertEquals(9, day2b(testString))
    }
}