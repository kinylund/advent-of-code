package advofcode2024

import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class Day6Test {
    val testString = """
            ....#.....
            .........#
            ..........
            ..#.......
            .......#..
            ..........
            .#..^.....
            ........#.
            #.........
            ......#...
        """.trimIndent()

    @Test
    fun testDay6a() {

        assertEquals(41, day6a(testString))
    }

    @Test
    fun testDay6b() {
        runBlocking {
            assertEquals(6, day6b(testString))
        }
    }
}