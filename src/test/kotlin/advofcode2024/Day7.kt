package advofcode2024

import kotlin.test.Test
import kotlin.test.assertEquals

class Day7Test {
    val testString = """
            156: 15 6
            190: 10 19
            3267: 81 40 27
            83: 17 5
            7290: 6 8 6 15
            161011: 16 10 13
            192: 17 8 14
            21037: 9 7 18 13
            292: 11 6 16 20
        """.trimIndent()
    @Test
    fun testDay7a() {
        assertEquals(3749, day7a(testString))
    }

    @Test
    fun testDay7b() {
        assertEquals(127, listOf(12, 7).joinToString("").toInt())
        assertEquals(11387, day7b(testString))
    }
}