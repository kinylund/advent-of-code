package advofcode2024

import kotlin.test.Test
import kotlin.test.assertEquals

class Day3Test {

    @Test
    fun testDay3a() {
        val testString = """
            xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))
        """.trimIndent()
        assertEquals(161, day3a(testString))
    }

    @Test
    fun testDay3b() {
        val testString = """
            xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))
        """.trimIndent()
        assertEquals(48, day3b(testString))
    }
}