package advofcode2021

import kotlin.test.Test
import kotlin.test.assertEquals

class Day2Test {

    @Test
    fun testDay2a() {
        val testString = """
            forward 5
            down 5
            forward 8
            up 3
            down 8
            forward 2
        """.trimIndent()
        assertEquals(150, day2a(testString))
    }

    @Test
    fun testDay2b() {
        val testString = """
            forward 5
            down 5
            forward 8
            up 3
            down 8
            forward 2
        """.trimIndent()
        assertEquals(900, day2b(testString))
    }
}