package advofcode2021

import kotlin.test.Test
import kotlin.test.assertEquals

class Day1Test {

    @Test
    fun testDay1a() {
        val testString = """
            199
            200
            208
            210
            200
            207
            240
            269
            260
            263
        """.trimIndent()
        assertEquals(7, day1a(testString))
    }

    @Test
    fun testDay1b() {
        val testString = """
            199
            200
            208
            210
            200
            207
            240
            269
            260
            263
        """.trimIndent()
        assertEquals(5, day1b(testString))
    }
}