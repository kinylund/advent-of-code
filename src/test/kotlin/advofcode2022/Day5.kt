package advofcode2022

import kotlin.test.Test
import kotlin.test.assertEquals

class Day5Test {

    @Test
    fun testDay5a() {
        val testString = """
    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
        """.trimIndent()
        assertEquals("CMZ", day5a(testString))
    }

    @Test
    fun testDay5b() {
        val testString = """
    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
        """.trimIndent()
        assertEquals("MCD", day5b(testString))
    }
}