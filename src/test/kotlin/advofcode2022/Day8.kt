package advofcode2022

import kotlin.test.Test
import kotlin.test.assertEquals

class Day8Test {
    val testString = """
            30373
            25512
            65332
            33549
            35390
        """.trimIndent()

    @Test
    fun testDay8a() {
        assertEquals(21, day8a(testString))
    }

    @Test
    fun testDay8b() {
        assertEquals(8, day8b(testString))
    }
}