package advofcode2022

import kotlin.test.Test
import kotlin.test.assertEquals

class Day1Test {

    @Test
    fun testDay1a() {
        val testString = """
            1000
            2000
            3000

            4000

            5000
            6000

            7000
            8000
            9000

            10000
        """.trimIndent()
        assertEquals(24000, day1a(testString))
    }

    @Test
    fun testDay1b() {
        val testString = """
                        1000
                        2000
                        3000

                        4000

                        5000
                        6000

                        7000
                        8000
                        9000

                        10000
        """.trimIndent()
        assertEquals(45000, day1b(testString))
    }
}
