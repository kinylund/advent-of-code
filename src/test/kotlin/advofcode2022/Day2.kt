package advofcode2022

import kotlin.test.Test
import kotlin.test.assertEquals

class Day2Test {

    @Test
    fun testDay2a() {
        val testString = """
            A Y
            B X
            C Z
        """.trimIndent()
        assertEquals(15, day2a(testString))
    }

    @Test
    fun testDay2b() {
        val testString = """
            A Y
            B X
            C Z
        """.trimIndent()
        assertEquals(12, day2b(testString))
    }
}