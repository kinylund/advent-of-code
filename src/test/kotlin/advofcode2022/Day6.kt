package advofcode2022

import kotlin.test.Test
import kotlin.test.assertEquals

class Day6Test {

    @Test
    fun testDay6a() {
        val testString = """
            bvwbjplbgvbhsrlpgdmjqwftvncz
        """.trimIndent()
        assertEquals(5, day6a(testString))
    }

    @Test
    fun testDay6b() {
        val testString = """
            mjqjpqmgbljsphdztnvjfqwrcgsmlb
        """.trimIndent()
        assertEquals(19, day6b(testString))
    }
}