package advofcode2022

import kotlin.test.Test
import kotlin.test.assertEquals

class Day9Test {
    val testString = """
......
......
......
......
H.....   
        """.trimIndent()
    @Test
    fun testDay9a() {

        assertEquals(-1, day9a(testString))
    }

    @Test
    fun testDay9b() {

        assertEquals(-1, day9b(testString))
    }
}