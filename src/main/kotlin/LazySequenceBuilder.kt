
class LazySequenceBuilder<T> {
    private val producers = mutableListOf<() -> T>()

    fun item(producer: () -> T) {
        producers += producer
    }

    fun build(): Sequence<T> {
        return producers.asSequence().map { it() }
    }
}

fun <T> lazySequence(block: LazySequenceBuilder<T>.() -> Unit): Sequence<T> {
    val builder = LazySequenceBuilder<T>()
    builder.block()
    return builder.build()
}