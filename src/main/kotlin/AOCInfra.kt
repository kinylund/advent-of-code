import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.coroutines.runBlocking
import java.io.File

fun readDayFile(year: Int, day: Int): String = runBlocking {
    val dataDir = File("data/$year")
    val file = File(dataDir, "day$day.txt")

    if (file.exists()) {
        file.readText()
    } else {
        val data = downloadDayInput(year, day)
        dataDir.mkdirs()
        file.writeText(data)
        data
    }
}

private fun readSessionToken(): String {
    val sessionFile = File(".session")
    if (!sessionFile.exists()) {
        throw IllegalStateException("Session file not found. Please create a .session file with your session token.")
    }
    return sessionFile.readText().trim()
}

private val client = HttpClient(CIO) {
    val session = readSessionToken()
    defaultRequest {
        header(HttpHeaders.Cookie, "session=$session")
    }
}

private suspend fun downloadDayInput(year: Int, day: Int): String {
    val response: HttpResponse = client.get("https://adventofcode.com/$year/day/$day/input")

    if (response.status == HttpStatusCode.OK) {
        return response.bodyAsText().trimEnd()
    } else {
        throw RuntimeException("Failed to download input data: HTTP ${response.status.value}")
    }
}