enum class Direction(val y: Int, val x: Int) {
    UP(-1, 0),
    RIGHT(0, 1),
    DOWN(1, 0),
    LEFT(0, -1),
    STOP(0, 0);

    fun toPair() = Pair(y, x)
    fun rotateSteps(steps: Int) = Direction.entries[((ordinal + 4 + (steps % 4)) % 4)]

    companion object {
        fun fromString(str: String): Direction {
            return when (str) {
                "U" -> UP
                "D" -> DOWN
                "L" -> LEFT
                "R" -> RIGHT
                else -> STOP
            }
        }
    }
}
