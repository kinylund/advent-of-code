import advofcode2024.*
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

import kotlin.system.measureTimeMillis
import kotlin.time.measureTime

/**
 * <a href="https://adventofcode.com/2024">AdventOfCode</a>
 */
suspend fun main(args: Array<String>) = coroutineScope {
    println("Advent of code 2024!")
    val code = AdvOfCode24()
    val time = measureTimeMillis {
        val daysJobs = listOf(
            code::day1,
            code::day2,
            code::day3,
            code::day4,
            code::day5,
            code::day6,
            code::day7,
            code::day8
        ).map { function -> launch { measureTimeMillis { function() }.also { println("${function.name}-time: $it ms") } } }
        daysJobs.joinAll()
    }
    launch {
        measureTime {
        }.also { println("dayX-time: $it") }
    }.join()
    println("Runtime: ${time / 1000} s")
}

class AdvOfCode24 {

    fun day1() {
        val dayData = readDayFile(1)
        println("Day1a: ${day1a(dayData)}")
        println("Day1b: ${day1b(dayData)}")
    }

    fun day2() {
        val dayData = readDayFile(2)
        println("Day2a: ${day2a(dayData)}")
        println("Day2b: ${day2b(dayData)}")
    }

    fun day3() {
        val dayData = readDayFile(3)
        println("Day3a: ${day3a(dayData)}")
        println("Day3b: ${day3b(dayData)}")
    }

    fun day4() {
        val dayData = readDayFile(4)
        println("Day4a: ${day4a(dayData)}")
        println("Day4b: ${day4b(dayData)}")
    }

    fun day5() {
        val dayData = readDayFile(5)
        println("Day5a: ${day5a(dayData)}")
        println("Day5b: ${day5b(dayData)}")
    }

    fun day6() {
        val dayData = readDayFile(6)
        runBlocking {
            println("Day6a: ${day6a(dayData)}")
            println("Day6b: ${day6b(dayData)}")
        }
    }

    fun day7() {
        val dayData = readDayFile(7)
        println("Day7a: ${day7a(dayData)}")
        println("Day7b: ${day7b(dayData)}")
    }
    
    fun day8() {
        val dayData = readDayFile(8)
        println("Day8a: ${day8a(dayData)}")
        println("Day8b: ${day8b(dayData)}")
    }

    companion object {
        fun readDayFile(day: Int): String =
            readDayFile(2024, day)
    }

}
