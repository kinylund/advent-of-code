package advofcode2024

import NEW_LINE
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Semaphore
import lazySequence
import pmap

data class AccState(val testValue: Long, val numbers: List<Long>, val acc: Long)

fun day7a(vals: String): Long {
    val testLines = parsePairs(vals)
    return testLines.filter { testLine ->
        calculateNext(AccState(testLine.first, testLine.second, 0L), false) == testLine.first
    }.toList().sumOf { it.first }
}

private fun parsePairs(vals: String): List<Pair<Long, List<Long>>> {
    return vals.split(NEW_LINE).map { lines ->
        lines.split(": ").let {
            Pair(it.first().toLong(), it.last().split(' ').map { it.toLong() })
        }
    }
}

private fun calculateNext(state: AccState, bVariant: Boolean): Long {
    if (state.numbers.isEmpty()) {
        return state.acc
    }
    if (state.acc > state.testValue) {
        return state.acc
    }
    val calculatorList = buildCalculatorListLazy(state, bVariant)
    return calculatorList.firstOrNull {
        it == state.testValue
    } ?: 0L
}

private fun buildCalculatorListLazy(state: AccState, bVariant: Boolean) =
    lazySequence {
        item {
            calculateNext(
                AccState(state.testValue, state.numbers.drop(1), state.acc + state.numbers.first()),
                bVariant
            )
        }
        item {
            calculateNext(
                AccState(state.testValue, state.numbers.drop(1), state.acc * state.numbers.first()),
                bVariant
            )
        }
        if (bVariant) item {
            calculateNext(
                AccState(
                    state.testValue,
                    state.numbers.drop(1),
                    listOf(state.acc, state.numbers.first()).joinToString("").toLong()
                ),
                true
            )
        }
    }

fun day7b(vals: String): Long {
    val testLines = parsePairs(vals)
    return runBlocking {
        testLines.pmap(Semaphore(3)) { testLine ->
            val result = calculateNext(AccState(testLine.first, testLine.second, 0L), true)
            if (result == testLine.first) result else 0
        }.sum()
    }
}

