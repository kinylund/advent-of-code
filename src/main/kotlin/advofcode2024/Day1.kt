package advofcode2024

import NEW_LINE
import rotateLeft
import rotateRight
import kotlin.math.abs

fun day1a(vals: String): Int {
    return vals.split(NEW_LINE)
        .map { it.split(' ') }
        .map { listOf(it.first().toInt(), it.last().toInt()) }
        .rotateRight()
        .map { it.sorted() }
        .rotateLeft().sumOf { abs(it.first() - it.last()) }
}

fun day1b(vals: String): Int {
    val (keys, values) = vals.split(NEW_LINE)
        .map { it.split(' ') }
        .map { listOf(it.first().toInt(), it.last().toInt()) }
        .rotateRight()
    val counter = keys.map { Pair(it, 0) }.toMap().toMutableMap()
    values.forEach { value ->
        counter[value]?.also {
            counter[value] = it + 1
        }
    }
    return keys.map {
        counter[it]!! * it
    }.sum()
}
