package advofcode2024

fun day3a(vals: String): Int {
    val pattern = "mul\\(\\d+,\\d+\\)".toRegex()
    return pattern.findAll(vals).sumOf {
        val multipliers = it.value.drop(4)
            .dropLast(1)
            .split(",")
            .map { it.toInt() }
        multipliers.first() * multipliers.last()
    }
}

fun day3b(vals: String): Int {
    val dont = Regex("""don't\(\)""")
    val dos = "do\\(\\)".toRegex()
    val pattern = Regex("""mul\(\d+,\d+\)""")
    val splits = dos.splitToSequence(vals)
        return splits.map {
            dont.split(it).first()
        }.sumOf {
            pattern.findAll(it).sumOf {
            val multipliers = it.value.drop(4)
                .dropLast(1)
                .split(",")
                .map { it.toInt() }
            multipliers.first() * multipliers.last()
        }}
}
