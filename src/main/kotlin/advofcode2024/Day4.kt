package advofcode2024

import Direction
import NEW_LINE
import matrixDirIterator
import kotlin.streams.toList

fun day4a(vals: String): Int {
    val grid = vals.split(NEW_LINE).map { it.chars().toList() }
    val searchDirs = listOf(
        Direction.UP.toPair(),
        Direction.RIGHT.toPair(),
        Direction.DOWN.toPair(),
        Direction.LEFT.toPair(),
        Pair(1, 1),
        Pair(1, -1),
        Pair(-1, -1),
        Pair(-1, 1)
    )
    return grid.indices.sumOf { col ->
        grid[col].indices.filter {
            grid[col][it].toChar() == 'X'
        }.sumOf { row ->
            searchDirs.map {
                val string = grid.matrixDirIterator(Pair(col, row), it)
                    .take(4)
                    .map { it.toChar() }.joinToString("")
                if (string == "XMAS") 1 else 0
            }.sum()
        }
    }
}

fun day4b(vals: String): Int {
    val grid = vals.split(NEW_LINE).map { it.chars().toList() }
    val searchDirs = listOf(
        Pair(1, 1),
        Pair(-1, -1),
        Pair(1, -1),
        Pair(-1, 1)
    )
    return grid.indices.map { col ->
        grid.indices.map { row ->
            if (grid[col][row].toChar() == 'A') {
                if (searchDirs.windowed(2, 2).all {
                    val (dir1, dir2) = it
                    val diagStrLost: List<Int> = listOf(
                        grid.matrixDirIterator(Pair(col, row), dir1).drop(1).take(1).toList(),
                        listOf(grid[col][row]),
                        grid.matrixDirIterator(Pair(col, row), dir2).drop(1).take(1).toList()
                    ).flatten()
                    val diagStr = diagStrLost.map { it.toChar() }.joinToString("")
                    diagStr in listOf("MAS", "SAM")
                }) 1 else 0
            } else 0
        }.sum()
    }.sum()
}


