package advofcode2024

import Direction
import NEW_LINE
import get
import matrixDirIteratorIndexed
import plus
import replaceAtIndex
import kotlin.streams.toList

fun day6a(vals: String): Int {
    val triple = setup(vals)
    val mapMatrix = triple.first
    val guardIndex = triple.second
    val guardDir = triple.third
    val visited = findVisited(guardIndex, mapMatrix, guardDir)
    return visited.count()
}

private fun findVisited(
    guardIndex: Pair<Int, Int>,
    mapMatrix: List<List<Char>>,
    guardDir: Direction
): MutableSet<Pair<Int, Int>> {
    var guardIndex1 = guardIndex
    var guardDir1 = guardDir
    val visited = mutableSetOf(guardIndex1)
    while (true) {
        val corridor = mapMatrix.matrixDirIteratorIndexed(guardIndex1, guardDir1).takeWhile {
            it.second != '#'
        }
        corridor.forEach {
            visited.add(it.first)
        }
        guardIndex1 = corridor.last().first
        if (mapMatrix.get(guardIndex1.plus(guardDir1)) != '#') {
            break
        }
        guardDir1 = guardDir1.rotateSteps(1)
    }
    return visited
}

fun day6b(vals: String): Int {
    val triple = setup(vals)
    val mapMatrix = triple.first
    val guardIndex = triple.second
    val guardDir = triple.third
    val visited = findVisited(guardIndex, mapMatrix, guardDir)
    return visited.filter { it != guardIndex }.fold(0) { acc, pos ->
        if (testForLoop(mapMatrix.replaceAtIndex(pos.first, pos.second, '#'), guardIndex, guardDir)) {
            acc + 1
        } else acc
    }
}

private fun testForLoop(
    mapMatrix: List<List<Char>>,
    initialGuardIndex: Pair<Int, Int>,
    initialGuardDir: Direction
): Boolean {
    val visited: MutableSet<Pair<Pair<Int, Int>, Direction>> = mutableSetOf()
    var guardIndex = initialGuardIndex
    var guardDir = initialGuardDir
    var isLoop = false
    while (!isLoop) {
        val corridor = mapMatrix.matrixDirIteratorIndexed(guardIndex, guardDir).takeWhile {
            it.second != '#'
        }
        guardIndex = corridor.last().first
        if (mapMatrix.get(guardIndex.plus(guardDir)) != '#') {
            break
        }
        corridor.forEach {
            if (!visited.add(Pair(it.first, guardDir))) {
                isLoop = true
            }
        }
        guardDir = guardDir.rotateSteps(1)
    }
    return isLoop
}

private fun setup(vals: String): Triple<List<List<Char>>, Pair<Int, Int>, Direction> {
    val guardChars = listOf('^', '<', '>', 'v')
    val mapMatrix = vals.split(NEW_LINE).map { it.chars().toList().map { it.toChar() } }
    val guardIndex = mapMatrix.mapIndexed { iy, row ->
        row.indexOfFirst { it in guardChars }.takeIf { it != -1 }?.let {
            Pair(iy, it)
        }
    }.firstNotNullOf { it }
    val guardDir = mapMatrix.get(guardIndex).let {
        when (it) {
            '^' -> Direction.UP
            '<' -> Direction.LEFT
            '>' -> Direction.RIGHT
            'v' -> Direction.DOWN
            else -> throw IllegalStateException()
        }
    }

    return Triple(mapMatrix, guardIndex, guardDir)
}
