package advofcode2024

import LINE_SEPARATOR
import NEW_LINE

fun day5a(vals: String): Int {
    val (rulesList, pagesLists) = parse(vals)
    val validPageLists = pagesLists.filter { pages ->
        val validRules = rulesList.filter { pages.containsAll(it) }
        validRules.all {
            pages.indexOf(it.first()) < pages.indexOf(it.last())
        }
    }
    return validPageLists.sumOf { it[it.size / 2] }
}

fun day5b(vals: String): Int {
    val (rulesList, pagesLists) = parse(vals)
    val invalidPageLists = pagesLists.filter { pages ->
        val validRules = rulesList.filter { pages.containsAll(it) }
        !validRules.all {
            pages.indexOf(it.first()) < pages.indexOf(it.last())
        }
    }
    val fixedPageLists = invalidPageLists.map { pages ->
        val validRules = rulesList.filter { pages.containsAll(it) }
        var allFixed = false
        val mutatedPages = pages.toMutableList()
        while (!allFixed) {
            val brokenRules = validRules.filter { mutatedPages.indexOf(it.first()) > mutatedPages.indexOf(it.last()) }
            brokenRules.take(1).forEach { rule ->
                val p1 = mutatedPages.indexOf(rule.first())
                val p2 = mutatedPages.indexOf(rule.last())
                mutatedPages.remove(rule.first())
                mutatedPages.remove(rule.last())
                mutatedPages.add(p2.coerceAtMost(mutatedPages.size), rule.first())
                mutatedPages.add(p1, rule.last())
            }
            allFixed = validRules.all {
                mutatedPages.indexOf(it.first()) < mutatedPages.indexOf(it.last())
            }
        }
        mutatedPages.toList()
    }
    return fixedPageLists.sumOf { it[it.size / 2] }
}

private fun parse(vals: String): Pair<List<List<Int>>, List<List<Int>>> {
    val (rules, data) = vals.split(LINE_SEPARATOR)
    val rulesList = rules.split(NEW_LINE).map { it.split("|").map { it.toInt() } }
    val pagesLists = data.split(NEW_LINE).map { it.split(",").map { it.toInt() } }
    return Pair(rulesList, pagesLists)
}
