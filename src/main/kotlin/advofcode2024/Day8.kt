package advofcode2024

import NEW_LINE
import combineAllPairs
import minus
import plus
import times
import kotlin.streams.toList

fun day8a(vals: String): Int {
    val (map, signals) = parse(vals)
    val antinodes = getAntinodePositions(signals, map, false)
    return antinodes.count()
}

private fun getAntinodePositions(
    signals: Map<Char, List<Pair<Int, Int>>>, map: List<List<Char>>, b: Boolean
): Set<Pair<Int, Int>> {
    return signals.values.map { signalPos ->
            val allSignals = signalPos.toSet()
            signalPos.combineAllPairs().map { antinodesWithinBounds(map, allSignals, it, b) }.takeIf { it.isNotEmpty() }
                ?.reduce { acc, pairs ->
                    acc.union(pairs)
                } ?: emptySet()
        }.reduce { acc, pairs -> acc.union(pairs) }
}

private fun parse(vals: String): Pair<List<List<Char>>, Map<Char, List<Pair<Int, Int>>>> {
    val map = vals.split(NEW_LINE).map { it.chars().toList().map { it.toChar() } }
    val signals = map.mapIndexed { y, row ->
        row.mapIndexed { x, signal ->
            Pair(Pair(y, x), signal)
        }.filter { it.second != '.' }
    }.flatten().groupBy({ it.second }, { it.first })
    return Pair(map, signals)
}

private fun antinodesWithinBounds(
    map: List<List<Char>>, allSignals: Set<Pair<Int, Int>>, pair: Pair<Pair<Int, Int>, Pair<Int, Int>>, b: Boolean
): Set<Pair<Int, Int>> {
    val (pos1, pos2) = pair
    val diff = pos1 - pos2
    val positions = listOf(pos1, pos2)
    val searchRange = if (!b) -1..1 else (-map.size..map.size)
    return positions.map { pos ->
        searchRange.map { range ->
            (range * diff) + pos
        }.filter { b || it !in allSignals }.filter { it.first in map.indices && it.second in map.first().indices }
    }.flatten().toSet()
}

fun day8b(vals: String): Int {
    val (map, signals) = parse(vals)
    val antinodes = getAntinodePositions(signals, map, true)
    return antinodes.count()
}
