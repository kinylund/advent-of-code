package advofcode2024

import NEW_LINE
import dropAt

fun day2a(vals: String): Int {
    return vals.split(NEW_LINE)
        .map {
            it.split(' ')
                .map { it.toInt() }
        }
        .map {
            isSafe(it, ::asc) || isSafe(it, ::desc)
        }.count { it }
}

private fun isSafe(it: List<Int>, safeFunc: (List<Int>) -> Boolean) = it.windowed(2)
    .all {
        safeFunc(it)
    }

fun day2b(vals: String): Int {
    return vals.split(NEW_LINE)
        .map {
            it.split(' ')
                .map { it.toInt() }
        }.map { slope ->
            isSafe(slope, ::asc) ||
                    isSafe(slope, ::desc) ||
                    slope.indices.any { index ->
                        slope.dropAt(index).let {
                            isSafe(it, ::asc) || isSafe(it, ::desc)
                        }
                    }
        }.count { it }
}

private fun asc(it: List<Int>) = it.first() < it.last() &&
        it.last() - it.first() in 1..3

private fun desc(it: List<Int>) = it.first() > it.last() &&
        it.first() - it.last() in 1..3
