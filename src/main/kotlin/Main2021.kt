import advofcode2021.*
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch

import kotlin.system.measureTimeMillis
import kotlin.time.measureTime

suspend fun main(args: Array<String>) = coroutineScope {
    println("Advent of code 2021!")
    val code = AdvOfCode21()
    val time = measureTimeMillis {
        val daysJobs = listOf(
            code::day1,
            code::day2,
            ).map { function -> launch { measureTimeMillis { function() }.also { println("${function.name}-time: $it ms") } } }
        daysJobs.joinAll()
    }
    launch {
        measureTime {
        }.also { println("dayX-time: $it") }
    }.join()
    println("Runtime: ${time / 1000} s")
}

class AdvOfCode21 {

    fun day1() {
        val dayData = readDayFile(1)
        println("Day1a: ${day1a(dayData)}")
        println("Day1b: ${day1b(dayData)}")
    }

    fun day2() {
        val dayData = readDayFile(2)
        println("Day2a: ${day2a(dayData)}")
        println("Day2b: ${day2b(dayData)}")
    }

    companion object {
        fun readDayFile(day: Int): String =
            readDayFile(2021, day)
    }

}
