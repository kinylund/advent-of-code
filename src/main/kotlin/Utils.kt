import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.sync.Semaphore
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

/**
 * Regexp for finding numbers
 */
val numberReader = "\\d+".toRegex()
val wordReader = "\\w+".toRegex()
const val NEW_LINE = "\n"
const val LINE_SEPARATOR = "\n\n"

typealias Position<T> = Pair<T, T>

suspend fun <A, B> Iterable<A>.pmap(semaphorePool: Semaphore? = null, f: suspend (A) -> B): List<B> = coroutineScope {
    val numSemaphore = Runtime.getRuntime().availableProcessors() - 1
    val semaphore = semaphorePool ?: Semaphore(numSemaphore)
    map {
        semaphore.acquire()
        async(Dispatchers.Default) {
            f(it).also {
                semaphore.release()
            }
        }
    }.awaitAll()
}

suspend fun <A, B> Iterable<A>.pmapIndexed(semaphorePool: Semaphore? = null, f: suspend (Int, A) -> B): List<B> =
    coroutineScope {
        val numSemaphore = Runtime.getRuntime().availableProcessors() - 1
        val semaphore = semaphorePool ?: Semaphore(numSemaphore)
        mapIndexed { index, elem ->
            semaphore.acquire()
            async(Dispatchers.Default) {
                f(index, elem).also {
                    semaphore.release()
                }
            }
        }.awaitAll()
    }

fun LongRange.chunk(size: Long): List<LongRange> =
    generateSequence(first) { (it + size) }.takeWhile { it < last + size }.map { it.coerceAtMost(last + 1) }
        .windowed(2) { (start, end) -> start..<end }.toList()

fun List<LongRange>.mergeAscending(): List<LongRange> {
    require(all { it.first <= it.last })
    var merged = false
    val newSequence = sortedBy { it.first }.windowed(2, step = 2) { (list1, list2) ->
        if (list1.last + 1 >= list2.first) {
            merged = true
            listOf(list1.first..list2.last.coerceAtLeast(list1.last))
        } else {
            listOf(list1, list2)
        }
    }.flatten()
    return if (merged) {
        newSequence.mergeAscending()
    } else this
}

/**
 * Partition from all the ranges and returns a list of intersection ranges
 * and a list of the non-matching ranges
 *
 * @receiver The lists to intersect from
 * @param other The intersecting list
 */
fun List<LongRange>.partition(other: LongRange): Pair<List<LongRange>, List<LongRange>> {
    return map {
        it.partition(other)
    }.fold(Pair(listOf(), listOf())) { acc, pair ->
        Pair(acc.first + listOfNotNull(pair.first), acc.second + pair.second)
    }
}

/**
 * partition this range with the other and return the intersection and a list of remaining ranges
 *
 * @receiver The list to partition
 * @param other The intersecting list
 *
 * @return Returns a pair of the intersecting range and a list of the non intersected ranges
 *         if no intersection the first item is null
 */
fun LongRange.partition(other: LongRange): Pair<LongRange?, List<LongRange>> {
    val intersectionStart = maxOf(first, other.first)
    val intersectionEnd = minOf(last, other.last)

    val intersection = when {
        intersectionStart <= intersectionEnd -> intersectionStart..intersectionEnd
        else -> return null to listOf(this)
    }

    val remainingRanges = mutableListOf<LongRange>()

    if (first < other.first) {
        remainingRanges.add(first..<intersectionStart)
    }

    if (last > other.last) {
        remainingRanges.add((intersectionEnd + 1)..last)
    }

    return intersection to remainingRanges
}

fun IntRange.isOverlapping(r2: IntRange) = max(this.first, r2.first) <= min(this.last, r2.last)

fun stringsToDigit(string: String): String {
    val digitStrings = listOf(
        "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"
    )
    val splitter = "(one|two|three|four|five|six|seven|eight|nine|\\d|\\n)".toRegex()
    val rewrittenString = string.indices.mapNotNull {
        splitter.matchAt(string, it)?.value
    }
    var replaced = rewrittenString.joinToString("")
    digitStrings.withIndex().forEach {
        replaced = replaced.replace(it.value.toRegex(), (it.index + 1).toString())
    }
    return replaced
}

fun findLCM(a: Long, b: Long): Long {
    val larger = max(a, b)
    val maxLcm = min(a * b, Long.MAX_VALUE / 1000)
    var lcm = larger
    while (lcm <= maxLcm) {
        if (lcm % a == 0L && lcm % b == 0L) {
            return lcm
        }
        lcm += larger
    }
    return maxLcm
}

fun findLCMOfList(numbers: List<Long>) =
    numbers.reduce { acc, l ->
        findLCM(acc, l)
    }


fun <T> List<List<T>>.transpose(): List<List<T>> {
    require(all { it.size == first().size }) {
        "Only symmetric matrix can be transposed"
    }
    val transposed: MutableList<MutableList<T>> = mutableListOf()

    repeat(first().size) {
        transposed.add(mutableListOf())
    }

    map {
        it.forEachIndexed { index, ts ->
            transposed[index].add(ts)
        }
    }
    return transposed.map {
        it.toList()
    }.toList()
}

fun <T> List<List<T>>.rotateLeft(num: Int): List<List<T>> {
    var matrix = this
    repeat(num) {
        matrix = matrix.rotateLeft()
    }
    return matrix
}

fun <T> List<List<T>>.rotateRight(num: Int): List<List<T>> {
    var matrix = this
    repeat(num) {
        matrix = matrix.rotateRight()
    }
    return matrix
}


fun <T> List<List<T>>.rotateRight(): List<List<T>> {
    require(all { it.size == first().size }) {
        "Only symmetric matrix can be rotated"
    }
    val rotated: MutableList<MutableList<T>> = mutableListOf()

    repeat(first().size) {
        rotated.add(mutableListOf())
    }

    reversed().map {
        it.forEachIndexed { index, ts ->
            rotated[index].add(ts)
        }
    }
    return rotated.map {
        it.toList()
    }.toList()
}

fun <T> List<List<T>>.rotateLeft(): List<List<T>> {
    require(all { it.size == first().size }) {
        "Only symmetric matrix can be rotated"
    }
    val rotated: MutableList<MutableList<T>> = mutableListOf()

    repeat(first().size) {
        rotated.add(mutableListOf())
    }

    map {
        it.reversed().forEachIndexed { index, ts ->
            rotated[index].add(ts)
        }
    }
    return rotated.map {
        it.toList()
    }.toList()
}

/**
 * Split the list to multiple lists where the matching objects are in a solo list
 */
fun <T> List<T>.splitList(match: T): List<List<T>> {
    val result = mutableListOf<List<T>>()
    val currentList = mutableListOf<T>()

    this.forEach { element ->
        if (element == match) {
            if (currentList.isNotEmpty()) {
                result.add(currentList.toList())
                currentList.clear()
            }
            result.add(listOf(element))
        } else {
            currentList.add(element)
        }
    }

    if (currentList.isNotEmpty()) {
        result.add(currentList.toList())
    }

    return result
}

fun List<Int>.findPeriod(): Int? {
    val period = sorted().take(2).let { (one, two) ->
        abs(one - two)
    }
    return if (
        windowed(2, 1, false).map { (first, second) ->
            (first - second) % period
        }.all { it == 0 }
    ) {
        period
    } else
        null
}

fun List<Long>.findPeriod(): Long? {
    val period = sorted().take(2).let { (one, two) ->
        abs(one - two)
    }
    return if (
        windowed(2, 1, false).map { (first, second) ->
            (first - second) % period
        }.all { it == 0L }
    ) {
        period
    } else
        null
}

fun <T> List<List<T>>.matrixDirIteratorIndexed(
    start: Pair<Int, Int>,
    direction: Direction,
) = matrixDirIteratorIndexed(start, direction.toPair())

fun <T> List<List<T>>.matrixDirIteratorIndexed(
    start: Pair<Int, Int>,
    direction: Pair<Int, Int>,
) = sequence {
    var currentPos: Pair<Int, Int>? = start
    while (currentPos != null) {
        val element = this@matrixDirIteratorIndexed.get(currentPos)
        if (element != null) {
            yield(Pair(currentPos, element))
            currentPos = currentPos.plus(direction)
        } else {
            break
        }
    }
}

fun <T> List<List<T>>.matrixDirIterator(
    start: Pair<Int, Int>,
    direction: Direction,
) = matrixDirIterator(start, direction.toPair())

fun <T> List<List<T>>.matrixDirIterator(
    start: Pair<Int, Int>,
    direction: Pair<Int, Int>,
): Sequence<T> = sequence {
    var currentPos: Pair<Int, Int>? = start
    while (currentPos != null) {
        val element = this@matrixDirIterator.get(currentPos)
        if (element != null) {
            yield(element)
            currentPos = currentPos.plus(direction)
        } else {
            break
        }
    }
}

fun <E> List<List<E>>.get(pos: Pair<Int, Int>): E? = this.getOrNull(pos.first)?.getOrNull(pos.second)


fun List<List<Int>>.matrixAddAllFrom(other: List<List<Int>>): List<List<Int>> {
    require(size == other.size) { "Dimensions mismatch" }
    return indices.map { row ->
        val our = get(row)
        val their = other[row]
        require(our.size == their.size) { "Dimensions mismatch" }
        our.zip(their) { a, b ->
            a + b
        }
    }
}

fun <T> createEmptyMutableMap(mapLines: List<List<Any>>, initial: T) = mapLines.indices.map {
    mapLines[it].indices.map {
        initial
    }.toMutableList()
}.toMutableList()

fun <T> createEmptyMutableMap(maxY: Int, maxX: Int, initial: T) = (0..maxY).map {
    (0..maxX).map {
        initial
    }.toMutableList()
}.toMutableList()

fun calculatePolygonAreaLong(trench: List<Pair<Long, Long>>): Double =
    (trench + trench.first()).windowed(2).fold(0L) { acc, (p1, p2) ->
        acc + p1.second * p2.first - p1.first * p2.second
    } * 0.5

fun calculatePolygonAreaInt(trench: List<Pair<Int, Int>>): Double =
    (trench + trench.first()).windowed(2).fold(0L) { acc, (p1, p2) ->
        acc + p1.second * p2.first - p1.first * p2.second
    } * 0.5

fun <T> List<T>.replaceAtIndex(col: Int, newItem: T) =
    mapIndexed { colIndex, chars -> if (col != colIndex) chars else newItem }

fun <T> List<List<T>>.replaceAtIndex(row: Int, col: Int, newItem: T) =
    mapIndexed { rowIndex, ts ->
        if (row != rowIndex) ts else ts.replaceAtIndex(col, newItem)
    }

fun Pair<Int, Int>.plus(dir: Direction): Pair<Int, Int> =
    first + dir.y to second + dir.x

operator fun Pair<Int, Int>.minus(other: Pair<Int, Int>) = Pair(first - other.first, second - other.second)
operator fun Pair<Int, Int>.plus(other: Pair<Int, Int>) = Pair(first + other.first, second + other.second)
operator fun Int.times(other: Pair<Int, Int>) = Pair(this * other.first, this * other.second)

fun <T> List<T>.combineAll(): List<List<T>> {
    return mapIndexed { index, item ->
        subList(index + 1, size).map { otherItem ->
            listOf(item, otherItem)
        }
    }.flatten()
}

fun <T> List<T>.combineAllPairs(): List<Pair<T, T>> {
    return mapIndexed { index, item ->
        subList(index + 1, size).map { otherItem ->
            item to otherItem
        }
    }.flatten()
}

fun <T> List<T>.combineAllFiltered(predicate: (T, T) -> Boolean): List<Pair<T, T>> {
    return mapIndexed { index, hailStone ->
        subList(index + 1, size).mapNotNull { otherHailStone ->
            if (predicate(hailStone, otherHailStone)) {
                hailStone to otherHailStone
            } else null
        }
    }.flatten()
}

fun <T> List<T>.partitionOne(): Pair<T, List<T>>? {
    return if (isEmpty()) {
        null
    } else {
        first() to subList(1, size)
    }
}

fun <T> List<T>.partitionN(elements: Int): Pair<List<T>, List<T>> {
    if (isEmpty()) return listOf<T>() to listOf()
    return subList(0, elements) to subList(elements, size)
}

fun <T> List<T>.dropAt(index: Int): List<T> {
    return toMutableList().apply {
        removeAt(index)
    }.toList()
}

fun <T> List<T>.partitionFirst(predicate: (T) -> Boolean): Pair<List<T>, List<T>> {
    if (isEmpty()) return listOf<T>() to listOf()
    val index = indexOfFirst(predicate)
    return if (index == -1) {
        this to listOf()
    } else {
        subList(0, index) to subList(index, size)
    }
}