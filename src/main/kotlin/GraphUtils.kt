fun breadthFirstTraverse(start: Node, putDistance: Boolean, blockedLinks: Set<Set<Node>>): Pair<Int, List<Node>> {
    val visitedNodes = mutableSetOf(start)
    var visitedNodesCount = 1
    var steps = 0
    var edgeNodes: List<Node> = listOf()
    do {
        val visitedNodesInStep = visitedNodes.toList().map { node ->
            node.connections.filterNot { it in visitedNodes || blockedLinks.contains(setOf(it, node)) }
                .map {
                    visitedNodes.add(it)
                    //it.heatMap[node.id] = it.heatMap.getOrDefault(node.id, 0) + 1
                    if (putDistance) {
                        it.steps = steps
                    }
                    it
                }
        }.flatten()
        visitedNodesCount += visitedNodesInStep.count()
        visitedNodesInStep.takeIf { it.isNotEmpty() }?.let {
            edgeNodes = it
            steps++
        }
    } while (visitedNodesInStep.isNotEmpty())
    // println("Nodes in $steps: $edgeNodes")
    return steps to visitedNodes.toList()
}

fun breadthFirstSearch(startEnd: Pair<Node,Node>, blockedLinks: Set<Set<Node>>): Pair<Int, List<Node>> {
    val (start, end) = startEnd
    val visitedNodes = mutableSetOf(start)
    var visitedNodesCount = 1
    var steps = 0
    do {
        val visitedNodesInStep = visitedNodes.toList().map { node ->
            node.connections.filterNot { it in visitedNodes || blockedLinks.contains(setOf(it, node)) }
                .map {
                    visitedNodes.add(it)
                    it.heatMap[node.id] = it.heatMap.getOrDefault(node.id, 0) + 1
                    it
                }
        }.flatten()
        visitedNodesCount += visitedNodesInStep.count()
        visitedNodesInStep.takeIf { it.isNotEmpty() }?.let {
            steps++
        }
    } while (visitedNodesInStep.isNotEmpty() && !visitedNodes.contains(end))
    // println("Nodes in $steps: $edgeNodes")
    return steps to visitedNodes.toList()
}

data class Node(val id: String) {
    var connections: Set<Node> = setOf()
    val edgeHeatMap: MutableMap<String, Int> = mutableMapOf()
    val heatMap: MutableMap<String, Int> = mutableMapOf()
    var cliques: Set<Set<Node>> = setOf()
    var steps: Int = 0
}

fun Node.createAllCliques() {
    val cliquesWip = connections.toList()
        .combineAllFiltered { one, other ->
            one.connections.contains(other)
        }
        .map { it.toList() + this }
        .map { it.toSet() }
    cliques = cliques.plus(cliquesWip)
    var level = 2
    do {
        val (ready, cliquesToExpand) = cliques.partition { it.size == level }
        val cliquesExpanded = cliquesToExpand.map {
            it.tryExpandCliques()
        }.flatten()
        cliques = cliquesToExpand.filterNot { oldCliqueItem ->
            cliquesExpanded.any { it.containsAll(oldCliqueItem) }
        }.toSet().plus(ready).plus(cliquesExpanded)
        level++
    } while (cliquesExpanded.isNotEmpty())
}

fun Set<Node>.tryExpandCliques(): List<Set<Node>> {
    val candidates = flatMap { node -> node.connections.filterNot { it in this } }
    return candidates.mapNotNull {
        if (it.connections.containsAll(this)) {
            this.plus(it)
        } else null
    }
}

fun depthFirstCountPaths(edge: Node, otherEdge: Node) {
    val visitedNodes = mutableSetOf(edge)
    do {

    } while (false)
}