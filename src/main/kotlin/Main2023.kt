import advofcode2023.*
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlin.system.measureTimeMillis
import kotlin.time.measureTime

suspend fun main(args: Array<String>) = coroutineScope {
    println("Advent of code 2023!")
    val code = AdvOfCode23()

    // println("Program arguments: ${args.joinToString()}")
    val time = measureTimeMillis {
        val daysJobs = listOf(
            code::day1,
            code::day2,
            code::day3,
            code::day4,
            code::day5,
            code::day6,
            code::day7,
            code::day8,
            code::day9,
            code::day10,
            code::day11,
            code::day12,
            code::day13,
            code::day14,
            code::day15,
            code::day16,
            code::day17,
            code::day18,
            code::day19,
            code::day20,
            code::day21,
            code::day22,
            code::day23,
            code::day24,
            code::day25
        ).map { function -> launch { measureTimeMillis { function() }.also { println("${function.name}-time: $it ms") } } }
        daysJobs.joinAll()
    }
    launch {
        measureTime {
        }.also { println("dayX-time: $it") }
    }.join()
    println("Runtime: ${time / 1000} s")
}

class AdvOfCode23 {

    fun day1() {
        val dayData = readDayFile(1)
        println("Day1a: ${day1a(dayData)}")
        println("Day1b: ${day1b(dayData)}")
    }

    fun day2() {
        val dayData = readDayFile(2)
        println("Day2a: ${day2a(dayData)}")
        println("Day2b: ${day2b(dayData)}")
    }

    fun day3() {
        val dayData = readDayFile(3)
        println("Day3a: ${day3a(dayData)}")
        println("Day3b: ${day3b(dayData)}")
    }

    fun day4() {
        val dayData = readDayFile(4)
        println("Day4a: ${day4a(dayData)}")

        val spaceSplitter = " +".toRegex()
        val dataPairs = dayData.split("\n")
            .map { row: String ->
                row.split(": ", " | ")
                    .map { it.trim() }
                    .drop(1)
                    .let { numbers ->
                        val winSet = numbers.last().split(spaceSplitter)
                            .map {
                                it.toInt()
                            }.toSet()
                        val selected = numbers.first().split(spaceSplitter)
                            .map {
                                it.toInt()
                            }.toSet()
                        winSet.intersect(selected).size
                    }
            }
        println("Day4b: ${day4b(dataPairs)}")
    }

    fun day5() {
        val dayData = readDayFile(5)
        println("Day5a: ${day5a(dayData)}")
        val result = day5b2(dayData)
        require(result == 34039469.toLong())
        println("Day5b: $result")
    }

    /*
    Time:        61     70     90     66
    Distance:   643   1184   1362   1041
     */
    fun day6() {
        val races = listOf(61L to 643L, 70L to 1184L, 90L to 1362L, 66L to 1041L)
        println("Day6a: ${day6(races)}")

        val maxTime = 61709066L
        val distanceToBeat = 643118413621041L
        println("Day6b: ${day6(listOf(maxTime to distanceToBeat))}")

    }

    fun day7() {
        val dayData = readDayFile(7)
        val resultA = day7a(dayData)
        require(252656917.toLong() == resultA)
        println("Day7a: $resultA")
        println("Day7b: ${day7b(dayData)}")
    }

    fun day8() {
        val dayDataA = readDayFile(8)
        val resultA = day8a(dayDataA)
        require(19637 == resultA)
        println("Day8a: $resultA")
        val dayDataB = readDayFile(8)
        val resultB = day8b(dayDataB)
        require(8811050362409L == resultB)
        println("Day8b: $resultB")
    }

    fun day9() {
        val dayData = readDayFile(9)
        val resultA = day9a(dayData)
        require(1762065988L == resultA)
        println("Day9a: $resultA")
        println("Day9b: ${day9b(dayData)}")
    }

    fun day10() {
        val dayData = readDayFile(10)
        val resultA = day10a(dayData)
        require(6923 == resultA)
        println("Day10a: $resultA")
        println("Day10b: ${day10b(dayData)}")
    }

    fun day11() {
        val dayData = readDayFile(11)
        val resultA = day11a(dayData)
        println("Day11a: $resultA")
        require(9947476 == resultA)
        println("Day11b: ${day11b(dayData, 1000000)}")
    }

    fun day12() {
        val dayData = readDayFile(12)
        runBlocking {
            val resultA = day12b(dayData, 1)
            println("Day12a: $resultA")
            require(8180L == resultA)
            val resultB = day12b(dayData, 5)
            println("Day12b: $resultB")
            require(620189727003627 == resultB)
        }
    }

    fun day13() {
        val dayData = readDayFile(13)
        val resultA = day13(dayData, true)
        println("Day13a: $resultA")
        require(39939 == resultA)
        println("Day13b: ${day13(dayData, false)}")
    }

    fun day14() {
        val dayData = readDayFile(14)
        val resultA = day14a(dayData)
        println("Day14a: $resultA")
        require(106997 == resultA)
        val resultB = day14b(dayData, 500)
        require(99641 == resultB)
        println("Day14b: $resultB")
    }

    fun day15() {
        val dayData = readDayFile(15)
        val resultA = day15a(dayData)
        println("Day15a: $resultA")
        require(508552 == resultA)
        println("Day15b: ${day15b(dayData)}")
    }

    fun day16() {
        val dayData = readDayFile(16)
        val resultA = day16a(dayData)
        println("Day16a: $resultA")
        require(7434 == resultA)
        println("Day16b: ${day16b(dayData)}")
    }

    fun day17() {
        val dayData = readDayFile(17)
        val resultA = day17(dayData, false)
        println("Day17a: $resultA")
        require(1039 == resultA)
        println("Day17b: ${day17(dayData, true)}")
    }

    fun day18() {
        val dayData = readDayFile(18)
        val resultA = day18a(dayData)
        println("Day18a: $resultA")
        require(53844L == resultA)
        println("Day18b: ${day18b(dayData)}")
    }

    fun day19() {
        val dayData = readDayFile(19)
        val resultA = day19a(dayData)
        println("Day19a: $resultA")
        require(432788L == resultA)
        println("Day19b: ${day19b(dayData)}")
    }

    fun day20() {
        val dayData = readDayFile(20)
        val resultA = day20a(dayData)
        println("Day20a: $resultA")
        require(814934624L == resultA)
        println("Day20b: ${day20b(dayData)}")
    }

    fun day21() {
        val dayData = readDayFile(21)
        val resultA = day21a(dayData, 64)
        println("Day21a: $resultA")
        require(3687 == resultA)
        println("Day21b: ${day21(dayData, 65)}")
        println("Day21b: ${day21(dayData, 65+131)}")
        println("Day21b: ${day21(dayData, 65+2*131)}")
        println("Day21b: ${day21(dayData, 65+3*131)}")
    }

    fun day22() = runBlocking {
        val dayData = readDayFile(22)
        val resultA = day22a(dayData)
        println("Day22a: $resultA")
        require(490 == resultA)
        println("Day22b: ${day22b(dayData)}")
    }

    fun day23() {
        val dayData = readDayFile(23)
        val resultA = day23(dayData, false)
        println("Day23a: $resultA")
        require(2218 == resultA)
        // println("Day24b: ${day23(dayData,true)}")
    }

    fun day24() {
        val dayData = readDayFile(24)
        val resultA = day24a(dayData, 200000000000000.toBigDecimal()..400000000000000.toBigDecimal())
        println("Day24a: $resultA")
        println("Day24b: ${day24b(dayData, 2L.toBigInteger().pow(12).toLong())}")
    }

    fun day25() {
        runBlocking {
            val dayData = readDayFile(25)
            val resultA = day25a(dayData)
            println("Day25a: $resultA")
        }
    }

    companion object {
        private fun readDayFile(day: Int) =
            readDayFile(2023, day)
    }

}
