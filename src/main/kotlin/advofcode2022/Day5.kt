package advofcode2022

import partitionN
import partitionOne
import rotateRight
import kotlin.streams.toList

fun day5a(vals: String): String {
    val (arangement, commands) = vals.split("\n\n").map { it.split("\n") }

    val piles = arangement.map {
        it.chars().toList()
    }.rotateRight()
        .filter { it.first() != 32 }
        .map {
            it.drop(1).filterNot { it == 32 }.reversed()
        }.toMutableList()

    val program = commands.map {
        it.split("( from )|( to )|(move )".toRegex()).filter { it.isNotEmpty() }.map { it.toInt() - 1 }
    }
    program.forEach {
        val (move, from, to) = it
        (0..move).forEach {
            val (item, rest) = piles[from].partitionOne()!!
            piles[to] = listOf(item) + piles[to]
            piles[from] = rest
        }
    }
    return piles.map { it.first() }.joinToString("") { it.toChar().toString() }
}

fun day5b(vals: String): String {
    val (arangement, commands) = vals.split("\n\n").map { it.split("\n") }

    val piles = arangement.map {
        it.chars().toList()
    }.rotateRight()
        .filter { it.first() != 32 }
        .map {
            it.drop(1).filterNot { it == 32 }.reversed()
        }.toMutableList()

    val program = commands.map {
        it.split("( from )|( to )|(move )".toRegex()).filter { it.isNotEmpty() }.map { it.toInt() - 1 }
    }
    program.forEach {
        val (move, from, to) = it
        val (item, rest) = piles[from].partitionN(move + 1)
        piles[to] = item + piles[to]
        piles[from] = rest
    }
    return piles.map { it.first() }.joinToString("") { it.toChar().toString() }
}
