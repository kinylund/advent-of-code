package advofcode2022

import isOverlapping

fun day4a(vals: String): Int {
    val pairs = vals.split("\n")
        .filter { it.isNotEmpty() }
        .map {
            it.split("-", ",").map { it.toInt() }
        }

    return pairs.count { rangePair ->
        val range1 = rangePair[0]..rangePair[1]
        val range2 = rangePair[2]..rangePair[3]
        val intersectionStart = minOf(range1.first, range2.first)
        val intersectionEnd = maxOf(range1.last, range2.last)
        val ends = listOf(intersectionEnd, intersectionStart)
        ends.all { range1.contains(it) } || ends.all { range2.contains(it) }
    }
}

fun day4b(vals: String): Int {
    val pairs = vals.split("\n")
        .filter { it.isNotEmpty() }
        .map {
            it.split("-", ",").map { it.toInt() }
        }

    return pairs.count { rangePair ->
        val range1 = rangePair[0]..rangePair[1]
        val range2 = rangePair[2]..rangePair[3]
        range1.isOverlapping(range2)
    }
}
