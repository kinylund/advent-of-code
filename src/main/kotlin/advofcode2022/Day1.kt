package advofcode2022

import splitList

fun day1a(vals: String): Int {
    return vals.split("\n")
        .map { it.toIntOrNull() }
        .splitList(null)
        .maxOfOrNull { it.sumOf { it ?: 0 } } ?: 0
}

fun day1b(vals: String): Int {
    return vals.split("\n")
        .map { it.toIntOrNull() }
        .splitList(null)
        .map { it.sumOf { it ?: 0 } }
        .sortedDescending().take(3)
        .sum()
}

