package advofcode2022

import partitionN
import kotlin.streams.toList

fun day3a(vals: String): Int {
    return vals.split("\n")
        .map {
            val chars = it.chars().toList()
            val compartments = chars.partitionN(chars.size / 2)
            compartments.first.toSet().intersect(compartments.second.toSet())
        }.sumOf { sackIntersect ->
            sackIntersect.sumOf {
                when (it) {
                    in 97..122 -> it - 96
                    in 65..90 -> it - 64 + 26
                    else -> {
                        0
                    }
                }
            }
        }
}

fun day3b(vals: String): Int {
    return vals.split("\n")
        .chunked(3)
        .map {
            it.drop(1)
                .fold(
                    it.first().chars().toList().toSet(),
                    { acc: Set<Int>, s: String ->
                        acc.intersect(s.chars().toList().toSet())
                    })
        }.sumOf { sackIntersect ->
            sackIntersect.sumOf {
                when (it) {
                    in 97..122 -> it - 96
                    in 65..90 -> it - 64 + 26
                    else -> {
                        0
                    }
                }
            }
        }
}
