package advofcode2022

enum class RockPaperScissors(val score: Int) {
    ROCK(1),
    PAPER(2),
    SCISSORS(3);

    companion object {
        fun from(value: String) =
            when (value) {
                "A",
                "X" -> ROCK

                "B",
                "Y" -> PAPER

                "C",
                "Z" -> SCISSORS

                else -> {
                    throw IllegalStateException(value)
                }
            }
    }
}

fun day2a(vals: String): Int {
    return vals.split("\n")
        .filter { it.isNotEmpty() }
        .map { it.split(" ") }
        .map { game ->
            game.map { RockPaperScissors.from(it) }
        }
        .map { game ->
            game.last().score + if (game.first() == game.last()) {
                3
            } else if (game.first() == RockPaperScissors.ROCK) {
                if (game.last() == RockPaperScissors.PAPER) {
                    6
                } else 0
            } else if (game.first() == RockPaperScissors.PAPER) {
                if (game.last() == RockPaperScissors.SCISSORS) {
                    6
                } else 0
            } else {
                if (game.last() == RockPaperScissors.ROCK) {
                    6
                } else 0
            }
        }.sum()
}

fun day2b(vals: String): Int {
    return vals.split("\n")
        .filter { it.isNotEmpty() }
        .map { it.split(" ") }
        .map { game ->
            Pair(RockPaperScissors.from(game.first()), game.last())
        }
        .map { game ->
            when (game.second) {
                "X" -> 0 + when(game.first) {
                    RockPaperScissors.ROCK -> RockPaperScissors.SCISSORS.score
                    RockPaperScissors.PAPER -> RockPaperScissors.ROCK.score
                    RockPaperScissors.SCISSORS -> RockPaperScissors.PAPER.score
                }
                "Y" -> 3 + game.first.score
                "Z" -> 6 + when(game.first) {
                    RockPaperScissors.ROCK -> RockPaperScissors.PAPER.score
                    RockPaperScissors.PAPER -> RockPaperScissors.SCISSORS.score
                    RockPaperScissors.SCISSORS -> RockPaperScissors.ROCK.score
                }
                else -> 0
            }
        }.sum()
}
