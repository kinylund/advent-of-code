package advofcode2022

fun day6a(vals: String): Int {
    val searchSize = 4
    return findContent(vals, searchSize)
}

fun day6b(vals: String): Int {
    val searchSize = 14
    return findContent(vals, searchSize)
}

private fun findContent(vals: String, searchSize: Int): Int {
    var index = 0
    vals.windowed(searchSize).takeWhile { it.toSet().count() != searchSize }.onEach { index++ }
    return searchSize + index
}
