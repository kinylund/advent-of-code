package advofcode2022

import Direction
import NEW_LINE
import Position
import createEmptyMutableMap
import get
import matrixAddAllFrom
import matrixDirIterator
import plus
import rotateLeft
import rotateRight

fun day8a(vals: String): Int {
    val treeMatrix = vals.split(NEW_LINE).map { it.map { it.digitToInt() } }
    var seenMatrix: List<List<Int>> = createEmptyMutableMap(treeMatrix, 0)
    repeat(4) { num ->
        val treeMatrixSide = treeMatrix.rotateRight(num)
        val result = countTreesOnSide(treeMatrixSide).rotateLeft(num)
        seenMatrix = seenMatrix.matrixAddAllFrom(result)
    }
    return seenMatrix.flatten().count { it > 0 }
}

private fun countTreesOnSide(treeMatrix: List<List<Int>>): List<List<Int>> {
    val seenMatrix = createEmptyMutableMap(treeMatrix, 0)
    treeMatrix.forEachIndexed { rowIndex, row ->
        row.foldIndexed(-1) { column: Int, acc: Int, i: Int ->
            if (i > acc) {
                seenMatrix[rowIndex][column]++
                i
            } else acc
        }
    }
    return seenMatrix
}

fun day8b(vals: String): Int {
    val treeMatrix = vals.split(NEW_LINE).map { it.map { it.digitToInt() } }
    val viewScore = treeMatrix.mapIndexed { rowIndex, row ->
        List(row.size) { colIndex ->
            listOf(Direction.UP, Direction.RIGHT, Direction.DOWN, Direction.LEFT).map {
                treeMatrix.lookDirection(it, Position(rowIndex, colIndex))
            }.reduce { acc, element -> acc * element }
        }
    }
    return viewScore.flatten().max()
}

private fun List<List<Int>>.lookDirection(dir: Direction, position: Position<Int>): Int {
    val height = this.get(position) ?: return 0
    val count = matrixDirIterator(position.plus(dir), dir).indexOfFirst { it >= height }.takeUnless { it == -1 }
        ?.let { it + 1 }
        ?: matrixDirIterator(position.plus(dir), dir).count()
    return count
}


