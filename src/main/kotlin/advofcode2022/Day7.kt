package advofcode2022

import NEW_LINE
import numberReader
import partitionFirst

data class Folder(
    val name: String,
    val parent: Folder? = null,
    val folderMap: MutableMap<String, Folder> = mutableMapOf(),
    val fileMap: MutableMap<String, Long> = mutableMapOf()
) {
    override fun toString(): String {
        return name
    }
}

fun day7a(vals: String): Long {
    val root = Folder("/")
    parseFileThree(root, vals)
    return findSizeLimit(root, 100000).sum()
}

private fun parseFileThree(root: Folder, vals: String) {
    var currFolder = root
    var terminalLines = vals.split(NEW_LINE)
    while (terminalLines.isNotEmpty()) {
        val command = terminalLines.first().trim()
        val (output, rest) = terminalLines.drop(1).partitionFirst { it.startsWith('$') }
        when {
            command == "$ ls" -> {
                updateTreeStruct(currFolder, output)
            }

            command.startsWith("\$ cd") -> {
                val (prompt, cmd, folderName) = command.split(" ")
                currFolder = if (folderName == "..") {
                    currFolder.parent ?: currFolder
                } else if (folderName == "/") {
                    root
                } else {
                    currFolder.folderMap[folderName] ?: currFolder
                }
            }
        }
        terminalLines = rest
    }
}

fun calcSize(folder: Folder): Long {
    return folder.folderMap.values.sumOf(::calcSize) + folder.fileMap.values.sum()
}

fun findSizeLimit(folder: Folder, limit: Long): List<Long> {
    val sizes = listOf(calcSize(folder)) + folder.folderMap.values.map {
        findSizeLimit(it, limit)
    }.flatten()
    return sizes.filter { it <= limit }
}

fun findBestDir(folder: Folder, limit: Long): Long? {
    val sizes = listOf(calcSize(folder)) + folder.folderMap.values.map {
        findBestDir(it, limit)
    }.filterNotNull()
    return sizes.filter { it >= limit }.minOrNull()
}

fun updateTreeStruct(currFolder: Folder, output: List<String>) {
    output.forEach {
        when {
            it.startsWith("dir ") -> {
                val folderName = it.split(" ").last()
                currFolder.folderMap.getOrPut(folderName) {
                    Folder(folderName, currFolder)
                }
            }

            numberReader.matchesAt(it, 0) -> {
                val (size, fileName) = it.split(" ")
                currFolder.fileMap.getOrPut(fileName) {
                    size.toLong()
                }
            }
        }
    }
}


fun day7b(vals: String): Long {
    val root = Folder("/")
    parseFileThree(root, vals)
    val freeSpaceNeeded = 30000000 - (70000000 - calcSize(root))
    return findBestDir(root, freeSpaceNeeded)!!
}
