import PokerHand.Companion.toPokerHand

sealed class PokerHand(val order: Int) {
    data class FiveOfKind(val type: Int) : PokerHand(6000)
    data class FourOfKind(val type: Int) : PokerHand(5000)
    data class FullHouse(val threeOfKind: ThreeOfKind, val onePair: OnePair) : PokerHand(4000)
    data class ThreeOfKind(val type: Int) : PokerHand(3000)
    data class TwoPair(val type1: Int, val type2: Int) : PokerHand(2000)
    data class OnePair(val type: Int) : PokerHand(1000)

    companion object {
        fun List<Pair<Int, Int>>.toPokerHand(): PokerHand? {
            return when {
                size == 5 -> {
                    null
                }

                size == 4 -> {
                    OnePair(maxBy { it.second }.first)
                }

                (size == 3 && this.any { it.second == 3 }) -> {
                    ThreeOfKind(first { it.second == 3 }.first)
                }

                size == 3 -> {
                    val (firstV, secondV) = filter { it.second == 2 }
                        .sortedByDescending { it.first }
                        .map { it.first }
                    TwoPair(firstV, secondV)
                }

                size == 2 -> {
                    when {
                        any { it.second == 1 } -> FourOfKind(maxBy { it.second }.first)
                        any { it.second == 3 } -> FullHouse(
                            ThreeOfKind(first { it.second == 3 }.first),
                            OnePair(first { it.second == 2 }.first)
                        )

                        else

                        -> null
                    }
                }

                size == 1 -> {
                    FiveOfKind(first().second)
                }

                else -> null
            }
        }
    }
}

class PokerHandComparatorSloppy : Comparator<PlayedPokerHand> {
    override fun compare(hand1: PlayedPokerHand, hand2: PlayedPokerHand): Int {
        val handResult = when {
            hand1.second == null && hand2.second == null -> 0
            hand1.second == null -> {
                0 - hand2.second!!.order
            }

            hand2.second == null -> {
                hand1.second!!.order
            }

            hand1.second!!::class != hand2.second!!::class -> {
                (hand1.second?.order ?: 0) - (hand2.second?.order ?: 0)
            }

            hand1.second!!::class == hand2.second!!::class -> {
                0
            }

            else -> throw IllegalStateException("There should be no hands like this")
        }

        if (handResult != 0) {
            return handResult
        }

        val highCard = hand1.first.zip(hand2.first)
            .map { (high1, high2) ->
                high1 - high2
            }.firstOrNull { it != 0 } ?: throw IllegalStateException("No ranking was found")

        return highCard
    }
}

fun List<Int>.findPokerHands(): PokerHand? {
    val cardLayout = Array(15) { 0 }
    forEach {
        cardLayout[it]++
    }
    val groupedCards = cardLayout
        .withIndex()
        .filter { it.value > 0 }
        .sortedWith(compareBy({ it.value }, { it.index })).reversed()
        .map {
            it.index to it.value
        }
    val highCardsJoker = groupedCards.firstOrNull { it.first == 1 }?.let { jokers ->
        if (groupedCards.size > 1) {
            groupedCards.filter { it.first != 1 }.let {
                val toIncrement = it.first()
                val bestHand = listOf(toIncrement.first to toIncrement.second + jokers.second) + it.drop(1)
                bestHand
            }
        } else groupedCards
    } ?: groupedCards

    val pokerHand = highCardsJoker.toPokerHand()
    return pokerHand
}

typealias PlayedPokerHand = Triple<List<Int>, PokerHand?, Int>
