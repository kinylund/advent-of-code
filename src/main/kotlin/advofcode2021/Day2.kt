package advofcode2021

import NEW_LINE

fun day2a(vals: String): Int {
    return vals.split(NEW_LINE)
        .fold(Pair(0, 0)) { acc, it ->
            val (command, value) = it.split(' ')
            when (command) {
                "up" -> Pair(acc.first - value.toInt(), acc.second)
                "down" -> Pair(acc.first + value.toInt(), acc.second)
                else -> Pair(acc.first, acc.second + value.toInt());
            }
        }.let { it.first * it.second }
}

fun day2b(vals: String): Int {
    return vals.split(NEW_LINE)
        .fold(Triple(0, 0, 0)) { acc, it ->
            val (command, value) = it.split(' ')
            when (command) {
                "up" -> Triple(acc.first - value.toInt(), acc.second, acc.third)
                "down" -> Triple(acc.first + value.toInt(), acc.second, acc.third)
                else -> Triple(acc.first, acc.second + value.toInt(), acc.third + acc.first * value.toInt())
            }
        }.let { it.second * it.third }
}
