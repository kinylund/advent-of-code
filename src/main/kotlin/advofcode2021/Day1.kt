package advofcode2021

import NEW_LINE

fun day1a(vals: String): Int {
    return vals.split(NEW_LINE)
        .map { it.toInt() }
        .windowed(2)
        .fold(0) { acc, i ->
            if (i[0] < i[1]) acc + 1
            else acc
        }
}

fun day1b(vals: String): Int {
    return vals.split(NEW_LINE)
        .map { it.toInt() }
        .windowed(3)
        .map { it.sum() }
        .windowed(2)
        .fold(0) { acc, i ->
            if (i[0] < i[1]) acc + 1
            else acc
        }
}
