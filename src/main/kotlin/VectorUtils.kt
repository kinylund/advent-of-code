import java.math.BigDecimal
import java.math.MathContext
import kotlin.math.acos


class VectorUtils {
}

typealias Degree = Double
typealias Radian = Double

fun Degree.toRadian(): Radian = this / 180 * Math.PI
fun Radian.toDegree(): Degree = this * 180 / Math.PI


fun checkIntersectXYFuture(
    hailStone: Pair<Triple<BigDecimal, BigDecimal, BigDecimal>, Triple<Double, Double, Double>>,
    otherHailStone: Pair<Triple<BigDecimal, BigDecimal, BigDecimal>, Triple<Double, Double, Double>>,
    intersection: Pair<BigDecimal, BigDecimal>
): Boolean {
    val (interX, interY) = intersection
    val future = listOf(
        (interX - hailStone.first.first) * hailStone.second.first.toBigDecimal(),
        (interX - otherHailStone.first.first) * otherHailStone.second.first.toBigDecimal(),
        (interY - hailStone.first.second) * hailStone.second.second.toBigDecimal(),
        (interY - otherHailStone.first.second) * otherHailStone.second.second.toBigDecimal()
    ).all { it > BigDecimal.ZERO }

    return future
}

fun Pair<Triple<BigDecimal, BigDecimal, BigDecimal>, Triple<Double, Double, Double>>.checkIntersectXY(
    other: Pair<Triple<BigDecimal, BigDecimal, BigDecimal>, Triple<Double, Double, Double>>,
    limit: ClosedRange<BigDecimal>
): Boolean {
    return try {
        val (x1, y1, z1) = first
        val (vx1, vy1, vz1) = second
        val (x2, y2, z2) = other.first
        val (vx2, vy2, vz2) = other.second

        // m1 = vy1 / vx1
        // m2 = vy2 / vx2
        // y  = m1(x -x1) + y1
        // y  = m2(x -x2) + y2
        // m1*x - m1*x1 + y1 = m2*x -m2*x2 + y2
        // m1*x - m2*x = y2 - y1 -m2*x2 + m1*x1
        val m1 = (vy1 / vx1)
        val m2 = (vy2 / vx2)

        val divider = (m1 - m2)
        val x = (y2 - y1 - m2.toBigDecimal() * x2 + m1.toBigDecimal() * x1) /
                divider.toBigDecimal()
        val y = m1.toBigDecimal() * (x - x1) + y1

        return (x in limit && y in limit).let {
            if (it) {
                // println("x = $x, y = $y")
                checkIntersectXYFuture(this, other, x to y)
            } else false
        }
    } catch (e: Exception) {
        false
    }
}

fun getAngleOfPoints(list: List<Pair<Triple<BigDecimal, BigDecimal, BigDecimal>, Triple<Double, Double, Double>>>): Double {
    val (a, b, c) = list.map { it.first }
    val v1 = getVectorFromPoints(a, b)
    val v2 = getVectorFromPoints(c, b)
    // Normalize v1
    val v1norm = v1.toNormalVector()

    // Normalize v2
    val v2norm = v2.toNormalVector()

    // Calculate the dot product
    val res = v1norm.first * v2norm.first + v1norm.second * v2norm.second + v1norm.third * v2norm.third

    // Recover the angle
    val angle = acos(res.toDouble())
    return angle
}

fun getVectorFromPoints(
    a: Triple<BigDecimal, BigDecimal, BigDecimal>,
    b: Triple<BigDecimal, BigDecimal, BigDecimal>
) = Triple(b.first - a.first, b.second - a.second, b.third - a.third)

fun Triple<BigDecimal, BigDecimal, BigDecimal>.toNormalVector(): Triple<BigDecimal, BigDecimal, BigDecimal> {
    val vMag = (first * first + second * second + third * third).sqrt(MathContext.DECIMAL128)
    val v2norm = Triple(
        first.divide(vMag, MathContext.DECIMAL64),
        second.divide(vMag, MathContext.DECIMAL64),
        third.divide(vMag, MathContext.DECIMAL64)
    )
    return v2norm
}

fun Pair<Triple<BigDecimal, BigDecimal, BigDecimal>, Triple<BigDecimal, BigDecimal, BigDecimal>>.travel(time: Long): Triple<BigDecimal, BigDecimal, BigDecimal> {
    val (x, y, z) = first
    val (vx, vy, vz) = second
    return Triple(
        x + (vx * time.toBigDecimal()),
        y + (vy * time.toBigDecimal()),
        z + (vz * time.toBigDecimal())
    )
}

fun Pair<Triple<BigDecimal, BigDecimal, BigDecimal>, Triple<BigDecimal, BigDecimal, BigDecimal>>.checkIntersectXYZ(
    other: Pair<Triple<BigDecimal, BigDecimal, BigDecimal>, Triple<BigDecimal, BigDecimal, BigDecimal>>,
): Triple<BigDecimal, BigDecimal, BigDecimal> {

    val tx =
        try {
            (other.first.first - this.first.first).divide(
                this.second.first - other.second.first, MathContext.DECIMAL128
            )
        } catch (e: ArithmeticException) {
            BigDecimal.ZERO
        }
    val ty =
        try {
            (other.first.second - this.first.second).divide(
                this.second.second - other.second.second, MathContext.DECIMAL128
            )
        } catch (e: ArithmeticException) {
            BigDecimal.ZERO
        }

    val tz =
        try {
            (other.first.third - this.first.third).divide(
                this.second.third - other.second.third, MathContext.DECIMAL128
            )
        } catch (e: ArithmeticException) {
            BigDecimal.ZERO
        }

    return Triple(tz, ty, tx)
}