package advofcode2023


fun day2a(testString: String): Int {
    return testString.split("\n").sumOf { game ->
        val values = game.split("[:,;]".toRegex()).map { it.trim() }
        val gameId = values.first().split(" ").last().toInt()
        var maxBlue = 0
        var maxRed = 0
        var maxGreen = 0
        values.drop(1).forEach {
            it.split(" ").also { colorValue ->
                when (colorValue.last()) {
                    "red" -> maxRed = colorValue.first().toInt().coerceAtLeast(maxRed)
                    "green" -> maxGreen = colorValue.first().toInt().coerceAtLeast(maxGreen)
                    "blue" -> maxBlue = colorValue.first().toInt().coerceAtLeast(maxBlue)
                    else -> throw IllegalArgumentException("${colorValue.last()} is not a valid color")
                }
            }
        }
        if (maxRed > 12 || maxGreen > 13 || maxBlue > 14) {
            0
        } else gameId
    }
}

fun day2b(testString: String): Long {
    return testString.split("\n").sumOf { game ->
        val values = game.split("[:,;]".toRegex()).map { it.trim() }
        var maxBlue = 0
        var maxRed = 0
        var maxGreen = 0
        values.drop(1).forEach {
            it.split(" ").also { colorValue ->
                when (colorValue.last()) {
                    "red" -> maxRed = colorValue.first().toInt().coerceAtLeast(maxRed)
                    "green" -> maxGreen = colorValue.first().toInt().coerceAtLeast(maxGreen)
                    "blue" -> maxBlue = colorValue.first().toInt().coerceAtLeast(maxBlue)
                    else -> throw IllegalArgumentException("${colorValue.last()} is not a valid color")
                }
            }
        }
        (maxBlue * maxRed * maxGreen).toLong()
    }
}
