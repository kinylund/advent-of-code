package advofcode2023

import Direction
import calculatePolygonAreaInt
import calculatePolygonAreaLong


fun day18a(dataString: String): Long {
    val trenchInfo = dataString.split("\n").map { it.split(" ") }
    var digger = 0 to 0
    val trench = trenchInfo.map { (dir, len, color) ->
        buildList {
            val nextDir = nextStepMapper(dir).toPair()
            repeat(len.toInt()) {
                digger = digger.first + nextDir.first to digger.second + nextDir.second
                add(digger)
            }
        }
    }.flatten()
    digger = 0 to 0
    val vector = trenchInfo.map { (dir, len, color) ->
        val nextDir = nextStepMapper(dir).toPair()
        digger = digger.first + nextDir.first * len.toInt() to digger.second + nextDir.second * len.toInt()
        digger
    }
    val areaVector = calculatePolygonAreaInt(vector)
    return 1 + trench.size / 2 + areaVector.toLong()
}

private fun nextStepMapper(dir: String): Direction {
    return when (dir) {
        "D" -> Direction.DOWN
        "L" -> Direction.LEFT
        "U" -> Direction.UP
        "R" -> Direction.RIGHT
        else -> throw IllegalStateException("Invalid direction $dir")
    }
}

fun day18b(dataString: String): Long {
    val trenchInfo = dataString.split("\n").map { it.split(" ") }
    var digger = 0L to 0L
    var trenchLen = 0L
    val vector = trenchInfo.map { (_, _, color) ->
        val colorWord = color.trim('(', ')', '#')
        val nextDir = nextStepDigitMapper(colorWord.takeLast(1)).toPair()
        val len = colorWord.dropLast(1).toLong(16)
        trenchLen += len
        digger = digger.first + nextDir.first * len to digger.second + nextDir.second * len
        digger
    }
    val areaVector = calculatePolygonAreaLong(vector)
    return 1 + trenchLen / 2 + areaVector.toLong()
}

private fun nextStepDigitMapper(dir: String): Direction {
    return when (dir) {
        "1" -> Direction.DOWN
        "2" -> Direction.LEFT
        "3" -> Direction.UP
        "0" -> Direction.RIGHT
        else -> throw IllegalStateException("Invalid direction $dir")
    }
}

fun <T : Any> String.mapToOp(term: Int, next: T, fail: T?): (Int) -> T? {
    return when (this) {
        "<" -> { a -> if (a < term) next else fail }
        ">" -> { a -> if (a > term) next else fail }
        else -> { a ->
            next
        }
    }
}
