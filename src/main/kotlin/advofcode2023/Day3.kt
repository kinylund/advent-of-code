package advofcode2023

fun day3a(testString: String): Int {
    val symbolMatcher = "[^\\w\\s.]".toRegex()
    val digitMatcher = "\\d+".toRegex()
    return testString.split("\n").let {
        listOf(".".repeat(it.first().length)) + it
    }.windowed(3, 1, true).filter {
        it.size > 1
    }.sumOf { block ->
        val searchLine = 1
        var sum = 0
        digitMatcher.findAll(block[searchLine]).forEach { matchResult ->
            val firstChar = (matchResult.range.first - 1).coerceAtLeast(0)
            val lastChar = (matchResult.range.last + 2).coerceAtMost(block[searchLine].length)
            val hasSymbol = block.map {
                symbolMatcher.containsMatchIn(it.subSequence(firstChar, lastChar))
            }.reduce { acc, b -> acc || b }
            if (hasSymbol) {
                sum += matchResult.value.toInt()
            }
        }
        sum
    }
}

fun day3b(testString: String): Int {
    val symbolMatcher = "[*]".toRegex()
    val digitMatcher = "\\d+".toRegex()
    return testString.split("\n").let {
        listOf(".".repeat(it.first().length)) + it
    }.windowed(3, 1, true).filter {
        it.size > 1
    }.sumOf { block ->
        val searchLine = 1
        symbolMatcher.findAll(block[searchLine]).map { matchResult ->
            val firstChar = (matchResult.range.first - 1).coerceAtLeast(0)
            val lastChar = (matchResult.range.last + 2).coerceAtMost(block[searchLine].length)
            block.map { rowString ->
                digitMatcher.findAll(rowString).filter { digitMatch ->
                    digitMatch.range.any { it in firstChar..<lastChar }
                }.map { it.value.toInt() }.toList()
            }.toList().flatten()
        }.filter { it.size > 1 }.map {
            it.reduce { acc, int -> acc * int }
        }.sum()
    }
}