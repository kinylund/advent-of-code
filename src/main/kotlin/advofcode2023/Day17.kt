package advofcode2023

import Direction
import createEmptyMutableMap
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Semaphore
import plus
import pmap

fun day17(dataString: String, isB: Boolean): Int {
    val islandData = dataString.split("\n").map { it.map { it.toString().toInt() } }
    val goal = islandData.lastIndex to islandData.first().lastIndex
    val costGraph = createEmptyMutableMap(islandData, 1000000)
    costGraph[goal.first][goal.second] = islandData[goal.first][goal.second]
    runBlocking {
        buildCostGraph((goal).plus(Direction.UP), islandData, costGraph)
    }
    val duplicateDepthCheck = if (!isB) 4 else 10
    return runBlocking {
        val numSemaphore = Runtime.getRuntime().availableProcessors() - 1
        val semaphorePool = Semaphore(numSemaphore)
        val agent17 = Agent17(islandData, costGraph, goal)
        var bestScore = Int.MAX_VALUE
        (5..if (!isB) 5 else 5).forEach { stepSize ->
            var goalCount = 0
            var agentList: List<AgentPosHeatScore> = listOf(AgentPosHeatScore(0 to 0, 0 to 0, listOf()))
            do {
                agentList = agentList.pmap(semaphorePool) {
                    if (!isB) {
                        agent17.stepAgentA(it.first, it.second.first, it.third, it.third.size + 3)
                    } else agent17.stepAgentB(it.first, it.second.first, it.third, it.third.size + stepSize)
                }.flatten()
                // Have a meeting...
                val (done, stepping) = agentList.partition { it.first == goal }
                bestScore = (done.minOfOrNull { it.second.first } ?: Int.MAX_VALUE).coerceAtMost(bestScore)
                goalCount += done.size
                agentList = stepping.groupBy { it.first }.values
                    .map {
                        removeDuplicates(it, duplicateDepthCheck, if (!isB) 10 else 20)
                    }
                    .flatten()
                    .filter { it.second.first <= bestScore }
                    .sortedBy { it.second.second }
                    .take(1000)
            } while (goalCount < 10000 && agentList.isNotEmpty())
        }
        bestScore
    }
}

private fun removeDuplicates(agentList: List<AgentPosHeatScore>, duplicateDepthCheck: Int, numOptionsToKeep: Int) =
    agentList
        .sortedBy { it.second.first }
        .fold(listOf<AgentPosHeatScore>()) { acc, agent ->
            if (!acc.any {
                    it.second.first == agent.second.first &&
                            it.third.takeLast(duplicateDepthCheck) == agent.third.takeLast(duplicateDepthCheck)
                }) {
                acc + agent
            } else acc
        }.take(numOptionsToKeep)

typealias AgentPosHeatScore = Triple<Pair<Int, Int>, Pair<Int, Int>, List<Direction>>

class Agent17(
    private val islandData: List<List<Int>>,
    private val optimalWalk: List<List<Int>>,
    private val goal: Pair<Int, Int>
) {
    fun stepAgentA(
        pos: Pair<Int, Int>,
        heat: Int,
        lastMoves: List<Direction>,
        stepLimit: Int
    ): List<AgentPosHeatScore> {
        if (pos == goal || lastMoves.size >= stepLimit) {
            return listOf(AgentPosHeatScore(pos, heat to heat + optimalWalk[pos.first][pos.second], lastMoves))
        }
        val forbidden = listOfNotNull(
            lastMoves.lastOrNull()?.rotateSteps(2),
            lastMoves.takeLast(3).takeIf { last3Steps ->
                last3Steps.size == 3 && last3Steps.all { it == lastMoves.last() }
            }?.last()
        )

        val directions = Direction.entries.take(4).filterNot { it in forbidden }
        return directions.mapNotNull {
            try {
                val nextPos = pos.plus(it)
                stepAgentA(
                    nextPos, heat + islandData[nextPos.first][nextPos.second],
                    lastMoves + it, stepLimit
                )
            } catch (e: IndexOutOfBoundsException) {
                null
            }
        }.flatten()
    }

    fun stepAgentB(
        pos: Pair<Int, Int>,
        heat: Int,
        lastMoves: List<Direction>,
        stepLimit: Int
    ): List<AgentPosHeatScore> {
        val lockedDir = lastMoves.takeLast(4).takeIf { last4Steps ->
            last4Steps.size == 1 ||
                    last4Steps.count { it == lastMoves.last() } < 4
        }?.lastOrNull()

        if (pos == goal) {
            return if (null == lockedDir) {
                listOf(AgentPosHeatScore(pos, heat to heat + optimalWalk[pos.first][pos.second], lastMoves))
            } else
                listOf()
        } else if (lastMoves.size >= stepLimit) {
            return listOf(
                AgentPosHeatScore(
                    pos,
                    heat to heat - (pos.first + pos.second) + optimalWalk[pos.first][pos.second],
                    lastMoves
                )
            )
        }

        val forbidden = listOfNotNull(
            lastMoves.lastOrNull()?.rotateSteps(2),
            lastMoves.takeLast(10).takeIf { last10Steps ->
                last10Steps.size == 10 && last10Steps.all { it == lastMoves.last() }
            }?.last(),
            lockedDir?.rotateSteps(-1),
            lockedDir?.rotateSteps(1)
        )

        val directions = Direction.entries.take(4).filterNot { it in forbidden }
        return directions.mapNotNull {
            try {
                val nextPos = pos.plus(it)
                stepAgentB(
                    nextPos, heat + islandData[nextPos.first][nextPos.second],
                    lastMoves + it, stepLimit
                )
            } catch (e: IndexOutOfBoundsException) {
                null
            }
        }.flatten()
    }
}

private suspend fun buildCostGraph(
    pos: Pair<Int, Int>,
    islandData: List<List<Int>>,
    dijkstra: MutableList<MutableList<Int>>
) {
    val allDirections = Direction.entries.take(4)
    val (row, col) = pos
    val heat = islandData[row][col]
    val minCost = allDirections.mapNotNull {
        try {
            dijkstra[row + it.y][col + it.x] + heat
        } catch (e: IndexOutOfBoundsException) {
            null
        }
    }.min()
    if (minCost < dijkstra[row][col]) {
        dijkstra[row][col] = minCost
        allDirections.pmap {
            try {
                buildCostGraph(pos.plus(it), islandData, dijkstra)
            } catch (e: IndexOutOfBoundsException) {
                Unit
            }
        }
    }
}
