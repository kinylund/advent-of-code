package advofcode2023

import isOverlapping
import partitionN
import pmap
import replaceAtIndex

fun day22a(dataString: String): Int {
    val tower = buildTower(dataString)
    var counter = 0
    tower.forEachIndexed { index, lists ->
        lists.forEach { brick ->
            val modifiedList: List<List<IntRange>> = lists.filterNot { it == brick }
            val brokenTower = tower.replaceAtIndex(index, modifiedList)
            if (brokenTower.findFallingBricks(brick).isEmpty()) {
                ++counter
            }
        }
    }

    return counter
}


suspend fun day22b(dataString: String): Int {
    val tower = buildTower(dataString)
    val count = tower.pmap { lists ->
        val sum = lists.sumOf { brick ->
            collapseTower(brick, tower)
        }
        //println("Removed level: ${lists.firstOrNull()?.last()?.first} -- $sum")
        sum
    }.sum()

    return count
}

private fun collapseTower(firstRemovedBrick: List<IntRange>, tower: List<List<List<IntRange>>>): Int {
    var counter = 0
    var brokenTower = tower
    var bricksToRemove = listOf(firstRemovedBrick)
    while (bricksToRemove.isNotEmpty()) {
        val removedBricksGroupedByFlor = bricksToRemove.groupBy { it.last().first }
        removedBricksGroupedByFlor.entries.forEach { (floor, bricks) ->
            val modifiedList: List<List<IntRange>> = brokenTower[floor].filterNot { it in bricks }
            brokenTower = brokenTower.replaceAtIndex(floor, modifiedList)
        }
        //println("Removing: $brick")
        bricksToRemove = brokenTower.findFallingBricksMulti(bricksToRemove)
        counter += bricksToRemove.size
    }
    return counter
}

private fun buildTower(dataString: String): List<List<List<IntRange>>> {
    val brickReader = """(\d+),(\d+),(\d+)~(\d+),(\d+),(\d+)""".toRegex()
    val bricks = dataString.split("\n").mapNotNull { brickString ->
        brickReader.matchEntire(brickString)?.let {
            it.destructured.toList().map {
                it.toInt()
            }
        }
    }
    val brickDescription = bricks.map { brickDesc ->
        val (first, second) = brickDesc.partitionN(3)
        first.zip(second) { a, b ->
            listOf(a, b).sorted().let {
                it.first()..it.last()
            }
        }
    }.sortedBy { it.last().first }
    val zMax = brickDescription.maxOf { it.last().last }
    val levels = (1..zMax).map { mutableListOf<List<IntRange>>() }

    brickDescription.forEach { brick ->
        var zIndex = brick.last().first
        while (zIndex > 1 && !levels[zIndex - 1].checkLevelCollision(brick) &&
            !levels.findZBricks(zIndex - 1)
                .any {
                    it.checkCollision(brick)
                }
        ) {
            --zIndex
        }
        val (x, y, z) = brick
        val height = 0..z.last - z.first
        levels[zIndex].add(listOf(x, y, zIndex..zIndex + height.last))
    }

    val tower = levels
        .map { level ->
            level.map { it.toList() }
        }
    return tower
}

/**
 * Checks if the removed brick causes any other falling bricks
 */
private fun List<List<List<IntRange>>>.findFallingBricks(removed: List<IntRange>): List<List<IntRange>> {
    val levelsToCheck = listOf(removed.last().last)
    return levelsToCheck.map { z ->
        this[z + 1].filter { brick ->
            !this.findZBricks(z).any { zBrick ->
                zBrick.checkCollision(brick)
            }
        }
    }.flatten()
}

/**
 * Checks if the removed brick causes any other falling bricks
 */
private fun List<List<List<IntRange>>>.findFallingBricksMulti(removed: List<List<IntRange>>): List<List<IntRange>> {
    val levelsToCheck = removed.map {
        it.last().last
    }.toSet()
    return levelsToCheck.flatMap { z ->
        this[z + 1].filter { brick ->
            !this.findZBricks(z).any { zBrick ->
                zBrick.checkCollision(brick)
            }
        }
    }
}

private fun List<List<List<IntRange>>>.findZBricks(levelNum: Int): List<List<IntRange>> {
    return (1..levelNum)
        .asSequence()
        .map { this[it] }
        .map {
            it.filter {
                it.last().contains(levelNum)
            }
        }.filter { it.isNotEmpty() }
        .flatten()
        .toList()
}

private fun List<IntRange>.checkCollision(brick: List<IntRange>): Boolean =
    this.zip(brick).partitionN(2).first.all {
        it.first.isOverlapping(it.second)
    }

private fun List<List<IntRange>>.checkLevelCollision(brick: List<IntRange>): Boolean =
    this.any { aBrick ->
        aBrick.checkCollision(brick)
    }

