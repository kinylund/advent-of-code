package advofcode2023

import findPeriod
import rotateLeft
import rotateRight
import splitList

fun day14a(dataString: String): Int {
    val disc = dataString.split("\n").map { row ->
        row.map { col -> col }
    }
    val tilted = disc.rotateRight().tilt(true).rotateLeft()
    //println(tilted.joinToString("\n"))
    return calculateLoad(tilted)
}

fun day14b(dataString: String, rotations: Int): Int {
    val disc = dataString.split("\n").map { row ->
        row.map { col -> col }
    }
    val loadCounter = mutableMapOf<Int, MutableList<Int>>()
    var rotatedDisc = disc
    repeat(rotations) {
        rotatedDisc =
            rotatedDisc.rotateRight().tilt(true).rotateRight().tilt(true).rotateRight().tilt(true).rotateRight()
                .tilt(true)
        val load = calculateLoad(rotatedDisc)
        //val hitter = findLCM(it.toLong(), rotations.toLong()).toInt()
        loadCounter[load] = loadCounter.getOrDefault(load, mutableListOf()).apply {
            add(it + 1)
        }
    }
    val loadCounterFiltered = loadCounter.filter {
        it.value.size > 10
    }
    val lcmPairs = loadCounterFiltered.entries.map {
        val period = it.value.drop(10).findPeriod() ?: Int.MAX_VALUE
        (1000000000 - it.value.last()) % period to it.key
    }.sortedBy { it.first }
    // println(rotatedDisc.joinToString("\n") + lcmPairs.first())
    return lcmPairs.firstOrNull { it.first == 0 }?.second ?: throw IllegalStateException("No pattern found! $lcmPairs")
}

private fun calculateLoad(rotatedDisc: List<List<Char>>) =
    rotatedDisc.reversed().foldIndexed(0) { index: Int, acc: Int, chars: List<Char> ->
        acc + (index + 1) * chars.count { it == 'O' }
    }

fun List<List<Char>>.tilt(right: Boolean = false): List<List<Char>> {
    return map { row ->
        row.splitList('#').map {
            if (right) {
                it.sorted()
            } else it.sortedDescending()
        }.flatten()
    }
}
