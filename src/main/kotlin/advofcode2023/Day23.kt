package advofcode2023

import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Semaphore
import pmap
import replaceAtIndex

fun day23(dataString: String, b: Boolean): Int {
    val mapMatrix = dataString.split("\n").map { lineStr ->
        lineStr.map { it }
    }
    val startPos = 0 to 1
    val endPos = mapMatrix.lastIndex to mapMatrix.last().lastIndex - 1
    val agentList = runBlocking {
        val numSemaphore = Runtime.getRuntime().availableProcessors() - 1
        val semaphorePool = Semaphore(numSemaphore)
        runAgent23(startPos, 0, mapMatrix, endPos, brave = b, semaphorePool)
    }
    return agentList.maxBy { it.second }.second
}

suspend fun runAgent23(
    startPos: Pair<Int, Int>,
    steps: Int,
    mapMatrix: List<List<Char>>,
    endPos: Pair<Int, Int>,
    brave: Boolean,
    semaphorePool: Semaphore
): List<Pair<Pair<Int, Int>, Int>> {
    var agentPos = startPos
    var agentSteps = 0
    var agentMatrix = mapMatrix
    val directions = Direction.entries.take(4)
    do {
        val options = directions.mapNotNull { dir ->
            try {
                val nextPos = agentPos.first + dir.y to agentPos.second + dir.x
                agentMatrix[nextPos.first][nextPos.second].takeIf {
                    when (it) {
                        '#' -> false
                        '.' -> true
                        '^' -> dir != Direction.DOWN || brave
                        '>' -> dir != Direction.LEFT || brave
                        'v' -> dir != Direction.UP || brave
                        '<' -> dir != Direction.RIGHT || brave
                        else -> {
                            false
                        }
                    }
                }?.let {
                    nextPos
                }
            } catch (e: IndexOutOfBoundsException) {
                null
            }
        }
        if (options.isEmpty()) {
            return emptyList()
        }
        agentMatrix = agentMatrix.replaceAtIndex(agentPos.first, agentPos.second, 'O')
        if (options.size == 1) {
            agentPos = options.first()
            agentSteps++
        } else {
            agentSteps++
            val pool = if (semaphorePool.availablePermits > 1) semaphorePool else Semaphore(1)
            return options.pmap(pool) {
                runAgent23(it, agentSteps + steps, agentMatrix, endPos, brave, semaphorePool)
            }.flatten().maxByOrNull { it.second }?.let { listOf(it) } ?: emptyList()
        }
    } while (agentPos != endPos)
    // println(agentMatrix.joinToString("\n") { it.joinToString("") } + "\n\n")
    return listOf(agentPos to agentSteps + steps)
}
