package advofcode2023

import Direction
import kotlinx.coroutines.runBlocking
import pmap
import java.awt.Dimension
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

// 5, 11, 55, 481843, 2409215, 5300273

fun day21a(dataString: String, steps: Int): Int {
    val gardenMatrix = dataString.split("\n").map { it.map { it } }
    val pathTiles = gardenMatrix.mapIndexed { rowIndex, row ->
        row.mapIndexed { colIndex, c ->
            c.takeIf {
                c in listOf('.', 'S')
            }?.let {
                Pair(rowIndex, colIndex)
            }
        }
    }.flatten().filterNotNull()
    val startTileY = gardenMatrix.indexOfFirst { row -> row.any { it == 'S' } }
    val startTileX = gardenMatrix[startTileY].indexOfFirst { it == 'S' }

    val validTiles = pathTiles.toSet()
    val directions = Direction.entries.take(4)
    var positions = setOf(startTileY to startTileX)
    repeat(steps) {
        positions = positions.map { position ->
            directions.mapNotNull { direction ->
                (position.first + direction.y to position.second + direction.x).takeIf {
                    validTiles.contains(it.let {
                        (it.first + gardenMatrix.size * 100) % gardenMatrix.size to
                                (gardenMatrix.first().size * 100 + it.second) % gardenMatrix.first().size
                    })
                }
            }
        }.flatten().toSet()
        //println("$it: " + positions.size)
    }
    // println(positions.sortedBy { it.first })
    return positions.size
}

data class TilePosition(val tileY: Int, val tileX: Int) {
    var visits = 0
}

fun day21(dataString: String, steps: Int): Int {
    val gardenMatrix = dataString.split("\n").map { it.map { it } }
    val pathTiles = gardenMatrix.mapIndexed { rowIndex, row ->
        row.mapIndexed { colIndex, c ->
            c.takeIf {
                c in listOf('.', 'S')
            }?.let {
                Pair(rowIndex, colIndex)
            }
        }
    }.flatten().filterNotNull()
    val startTileY = gardenMatrix.indexOfFirst { row -> row.any { it == 'S' } }
    val startTileX = gardenMatrix[startTileY].indexOfFirst { it == 'S' }
    val gardenSize = gardenMatrix.size to gardenMatrix.first().size
    val validTiles = pathTiles.toSet()
    val positionsEven: MutableSet<TilePosition> = mutableSetOf(TilePosition(startTileY, startTileX))
    val positionsOdd: MutableSet<TilePosition> = mutableSetOf()
    var newEven = positionsEven.toSet()
    var newOdd = positionsOdd.toSet()
    runBlocking {
        repeat(steps) { stepN ->
            if (stepN % 2 == 0) {
                newOdd = newEven.stepFlip(gardenSize, validTiles, newOdd)
                positionsOdd.addAll(newOdd)
            } else {
                newEven = newOdd.stepFlip(gardenSize, validTiles, newEven)
                positionsEven.addAll(newEven)
            }
        }
    }
    val result = if (steps % 2 == 0) {
        createImage(positionsEven, steps)
        positionsEven.size
    } else {
        createImage(positionsOdd, steps)
        positionsOdd.size
    }
    return result
}

private fun createImage(positions: Set<TilePosition>, step: Int) {
    val minX = positions.minBy { it.tileX }.tileX
    val minY = positions.minBy { it.tileY }.tileY
    val size = Dimension(positions.maxBy { it.tileX }.tileX - minX, positions.maxBy { it.tileY }.tileY - minY)
    val img = BufferedImage(size.width + 1, size.height + 1, BufferedImage.TYPE_INT_RGB)
    positions.asSequence().forEach {
        img.setRGB(it.tileX - minX, it.tileY - minY, 0xff0000)
    }
    ImageIO.write(img, "BMP", File("solution-$step.bmp"))
}

suspend fun Set<TilePosition>.stepFlip(
    gardenSize: Pair<Int, Int>,
    validTiles: Set<Pair<Int, Int>>,
    otherSet: Set<TilePosition>
): Set<TilePosition> {
    val directions = Direction.entries.take(4)
    return asSequence().filter { it.visits < 1 }.chunked(100).toList().pmap { positions ->
        positions.flatMap { position ->
            directions.mapNotNull { direction ->
                TilePosition(position.tileY + direction.y, position.tileX + direction.x).takeIf { tile ->
                    validTiles.contains(tile.let {
                        (it.tileY + gardenSize.first * 1000000) % gardenSize.first to
                                (gardenSize.second * 10000000 + it.tileX) % gardenSize.second
                    })
                }
            }.map { tile ->
                otherSet.indexOf(tile).takeUnless { it == -1 }?.let { elementIndex ->
                    otherSet.elementAt(elementIndex).apply {
                        visits++
                    }
                } ?: tile
            }
        }
    }.flatten().toSet()
}
