package advofcode2023

import chunk
import mergeAscending
import numberReader
import partition
import pmap

typealias DescItem = Triple<Long, Long, Long>

fun day5a(dataString: String): Long {
    val lines = dataString.split("\n")
    val startSeeds = numberReader.findAll(lines.first()).map { it.value.toLong() }.toList()
    val descMaps = parseDescMaps(lines.drop(1))
    val destList = startSeeds.map {
        descMaps.fold(it) { acc, triples ->
            triples.findValueInMap(acc)
        }
    }.toList()
    return destList.minOf { it }
}

fun parseDescMaps(lines: List<String>): List<List<DescItem>> {
    val descMapsMutable = mutableListOf<List<DescItem>>()
    var toBeSplit = lines
    while (toBeSplit.isNotEmpty()) {
        val take = toBeSplit.dropWhile { !numberReader.containsMatchIn(it) }
        val descItems = take.takeWhile { numberReader.containsMatchIn(it) }.map { descLine ->
            val (dest, source, range) = numberReader.findAll(descLine).map { it.value.toLong() }.toList()
            DescItem(dest, source, range)
        }
        descMapsMutable.add(descItems.sortedBy { it.second })
        toBeSplit = take.drop(descItems.size)
    }

    return descMapsMutable.toList()
}

fun List<DescItem>.findValueInMap(source: Long): Long {
    val destItem =
        firstOrNull { it: DescItem -> it.second <= source && source < (it.second + it.third) } ?: return source
    val delta = source - destItem.second
    return destItem.first + delta
}

fun findValueInMapRecursive(sources: List<LongRange>, descItemMaps: List<List<DescItem>>): Long {
    if (descItemMaps.isEmpty()) {
        return sources.minBy { it.first }.first
    }
    val descItems = descItemMaps.first()
    val descItemsRest = descItemMaps.drop(1)

    val sum = sources.foldRight(Long.MAX_VALUE) { sourceRange, acc ->
        var nextAcc = acc
        val nonMatchingSources = descItems.foldRight(listOf(sourceRange)) { destMap, sources ->
            val (intersect, sourcesRest) = sources.partition(destMap.second..destMap.second + destMap.third)
            if (intersect.isNotEmpty()) {
                val delta = destMap.first - destMap.second
                val next = intersect.map {
                    it.first + delta..it.last + delta
                }
                nextAcc = nextAcc.coerceAtMost(findValueInMapRecursive(next, descItemsRest))
            }
            sourcesRest
        }
        if (nonMatchingSources.isNotEmpty()) {
            nextAcc.coerceAtMost(findValueInMapRecursive(nonMatchingSources, descItemsRest))
        } else {
            nextAcc
        }
    }
    return sum
}

/**
 * Bruteforce approach
 */
suspend fun day5b(dataString: String): Long {
    val lines = dataString.split("\n")
    val startSeeds = numberReader.findAll(lines.first()).map { it.value.toLong() }.toList().chunked(2)
    val descMaps = parseDescMaps(lines.drop(1))
    val destList = startSeeds.map { (it.first()..<it.first() + it.last()) }.mergeAscending().map {
        it.chunk(10000000)
    }.flatten().pmap() { range ->
        range.fold(Long.MAX_VALUE) { max, source ->
            descMaps.fold(source) { acc, triples ->
                triples.findValueInMap(acc)
            }.coerceAtMost(max)
        }
    }
    return destList.minOf { it }
}

/**
 * Recursive range approach
 */
fun day5b2(dataString: String): Long {
    val lines = dataString.split("\n")
    val startSeeds = numberReader.findAll(lines.first()).map { it.value.toLong() }.toList().chunked(2)
    val descMaps = parseDescMaps(lines.drop(1))
    return startSeeds.map { (it.first()..<it.first() + it.last()) }.mergeAscending().let { ranges ->
        findValueInMapRecursive(ranges, descMaps)
    }
}
