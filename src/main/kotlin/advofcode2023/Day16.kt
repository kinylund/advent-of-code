package advofcode2023

import Direction
import createEmptyMutableMap

typealias Beam = Triple<Direction, Int, Int>

fun day16a(dataString: String): Int {
    val contraption = dataString.split("\n").map { row ->
        row.map { col -> col }
    }

    val beam: Beam = Beam(Direction.RIGHT, 0, 0)
    val traceMap = createEmptyMutableMap(contraption, 0)
    traceBeam(contraption, beam, traceMap)
    return traceMap.sumOf { it.count { it > 0 } }
}

fun day16b(dataString: String): Int {
    val contraption = dataString.split("\n").map { row ->
        row.map { col -> col }
    }
    val pos = 100
    val left = contraption.indices.map { getEnergyValue(Beam(Direction.RIGHT, it, 0), contraption) }.max()
    val right =
        contraption.indices.map { getEnergyValue(Beam(Direction.LEFT, it, contraption.first().size - 1), contraption) }
            .max()
    val up = contraption.first().indices.map { getEnergyValue(Beam(Direction.DOWN, 0, it), contraption) }.max()
    val down =
        contraption.first().indices.map { getEnergyValue(Beam(Direction.UP, contraption.size - 1, it), contraption) }
            .max()
    return listOf(left, right, up, down).max()
}

private fun getEnergyValue(beam: Beam, contraption: List<List<Char>>): Int {
    val traceMap = createEmptyMutableMap(contraption, 0)
    traceBeam(contraption, beam, traceMap)
    return traceMap.sumOf { it.count { it > 0 } }
}

fun traceBeam(contraption: List<List<Char>>, beam: Beam, traceMap: MutableList<MutableList<Int>>) {
    var beamPos: Pair<Int, Int> = beam.second to beam.third
    var nextDir = beam.first
    do {
        try {
            beamPos = beamLine(nextDir, beamPos.first, beamPos.second, contraption, traceMap)
        } catch (e: IndexOutOfBoundsException) {
            break
        }
        val element = contraption[beamPos.first][beamPos.second]
        nextDir = when (element) {
            '\\' -> {
                val step = if (nextDir == Direction.RIGHT || nextDir == Direction.LEFT) 1 else -1
                nextDir.rotateSteps(step)
            }

            '/' -> {
                val step = if (nextDir == Direction.RIGHT || nextDir == Direction.LEFT) -1 else 1
                nextDir.rotateSteps(step)
            }

            '-', '|' -> {
                if (traceMap[beamPos.first][beamPos.second] > 10000) {
                    Direction.STOP
                } else {
                    traceMap[beamPos.first][beamPos.second] += 10000
                    traceBeam(contraption, Beam(nextDir.rotateSteps(1), beamPos.first, beamPos.second), traceMap)
                    traceBeam(contraption, Beam(nextDir.rotateSteps(-1), beamPos.first, beamPos.second), traceMap)
                }
                Direction.STOP
            }

            else -> {
                Direction.STOP
            }
        }
    } while (nextDir != Direction.STOP)
}


private fun beamLine(
    beamDir: Direction,
    yPos: Int,
    xPos: Int,
    worldMap: List<List<Char>>,
    lightMap: MutableList<MutableList<Int>>
): Pair<Int, Int> {
    lightMap[yPos][xPos]++
    var beamPos = Pair(yPos + beamDir.y, xPos + beamDir.x)
    while (when (worldMap[beamPos.first][beamPos.second]) {
            '.' -> true
            '|' -> beamDir == Direction.DOWN || beamDir == Direction.UP
            '-' -> beamDir == Direction.LEFT || beamDir == Direction.RIGHT
            else -> false
        }
    ) {
        lightMap[beamPos.first][beamPos.second]++
        beamPos = Pair(beamPos.first + beamDir.y, beamPos.second + beamDir.x)
    }
    return beamPos
}