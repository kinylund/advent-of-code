package advofcode2023

import transpose

fun day13(dataString: String, isA: Boolean): Int {
    val mirrors = dataString.split("\n\n").map { it.split("\n") }.map { mirror ->
        mirror.map { row ->
            row.map {
                it
            }
        }
    }
    return mirrors.mapIndexed { index, mirror ->
        //println(mirror.joinToString("\n") { it.joinToString("") })
        val pair = if (isA) {
            findReflectionIndex(mirror, ::isMirror) to findReflectionIndex(mirror.transpose(), ::isMirror)
        } else {
            findReflectionIndex(mirror, ::isSmudgedMirror) to findReflectionIndex(mirror.transpose(), ::isSmudgedMirror)
        }
        val sum = pair.first * 100 + pair.second
        if (sum == 0 || (pair.first != 0 && pair.second != 0)) {
            println("Broken mirror: $index")
            println(mirror.joinToString("\n") { it.joinToString("") })
            throw IllegalStateException("No mirror found")
        }
        sum
    }.sum()
}

fun findReflectionIndex(mirror: List<List<Char>>, isMirrorFunction: (List<List<Char>>, Int) -> Boolean): Int {
    return (0..<mirror.size - 1).sumOf {
        if (isMirrorFunction(mirror.drop(it), 0)) {
            it + (mirror.size - it) / 2
        } else if (isMirrorFunction(mirror.dropLast(it), 0)) {
            (mirror.size - it) / 2
        } else 0
    }
}

fun isMirror(chunk: List<List<Char>>, smudges: Int): Boolean {
    if (chunk.isEmpty()) {
        return true
    } else if (chunk.size % 2 != 0) {
        return false
    }

    return if (chunk.first() == chunk.last()) {
        return isMirror(chunk.drop(1).dropLast(1), 0)
    } else false
}

fun isSmudgedMirror(chunk: List<List<Char>>, smudges: Int): Boolean {
    if (chunk.isEmpty()) {
        return smudges == 1
    } else if (chunk.size % 2 != 0) {
        return false
    }

    val diffValue = chunk.first().size - chunk.first().zip(chunk.last()).count { it.first == it.second }
    return if (diffValue <= 1) {
        return isSmudgedMirror(chunk.drop(1).dropLast(1), smudges + diffValue)
    } else false
}