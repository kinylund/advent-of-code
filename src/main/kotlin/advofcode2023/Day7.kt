package advofcode2023

import PlayedPokerHand
import PokerHandComparatorSloppy
import findPokerHands

fun day7a(dataString: String): Long {
    val hands = dataString.split("\n").map {
        it.split(" ")
    }.map { (cards, bet) ->
        val cardValues = cards.map {
            when (it) {
                'A' -> 14
                'K' -> 13
                'Q' -> 12
                'J' -> 11
                'T' -> 10
                else -> it.digitToInt()
            }
        }
        val pokerHand = cardValues.findPokerHands()
        PlayedPokerHand(cardValues, pokerHand, bet.toInt())
    }
    val orderedHands = hands.sortedWith(PokerHandComparatorSloppy())
    return orderedHands.withIndex().sumOf {
        (it.index.toLong() + 1L) * it.value.third.toLong()
    }
}

fun day7b(dataString: String): Long {
    val hands = dataString.split("\n").map {
        it.split(" ")
    }.map { (cards, bet) ->
        val cardValues = cards.map {
            when (it) {
                'A' -> 14
                'K' -> 13
                'Q' -> 12
                'J' -> 1
                'T' -> 10
                else -> it.digitToInt()
            }
        }
        val pokerHand = cardValues.findPokerHands()
        PlayedPokerHand(cardValues, pokerHand, bet.toInt())
    }
    val orderedHands = hands.sortedWith(PokerHandComparatorSloppy())
    return orderedHands.withIndex().sumOf {
        (it.index.toLong() + 1L) * it.value.third.toLong()
    }
}
