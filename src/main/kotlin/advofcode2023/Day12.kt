package advofcode2023

import pmap
import replaceAtIndex
import splitList


fun day12(dataString: String, unfold: Int): Int {
    val springRecords = dataString.split("\n")

    return springRecords.sumOf { recordPair ->
        val (record, notes) = recordPair.split(" ").let { it ->
            val (first, second) = it
            Pair((1..unfold).joinToString("?") { first }, (1..unfold).joinToString(",") { second })
        }
        val notesMatcher = notes.split(",").joinToString(".+") {
            "#".repeat(it.toInt())
        }
        val springMatcher = ".*$notesMatcher.*".toRegex()
        val locked = record.count { it == '#' }
        val recordList = record.map { it }.splitList('?')
        val wanted = notesMatcher.count { it == '#' }
        countCombinations(springMatcher, recordList, locked, wanted)
    }
}

fun countCombinations(springMatcher: Regex, brokenRecord: List<List<Char>>, locked: Int, wanted: Int): Int {
    if (locked == wanted) {
        val testString = brokenRecord.flatten().joinToString("")
        val matches: Boolean = springMatcher.matches(testString)
        return if (matches) 1 else 0
    }
    return brokenRecord.indices.firstOrNull { brokenRecord[it] == listOf('?') }?.let {
        countCombinations(
            springMatcher,
            brokenRecord.replaceAtIndex(it, listOf('#')),
            locked + 1,
            wanted
        ) + countCombinations(springMatcher, brokenRecord.replaceAtIndex(it, listOf('.')), locked, wanted)
    } ?: 0
}

suspend fun day12b(dataString: String, unfold: Int): Long {
    val springRecords = dataString.split("\n")
    return springRecords.pmap() { recordPair ->
        val (record, notes) = recordPair.split(" ").let {
            val (first, second) = it
            Pair((1..unfold).joinToString("?") { first }, (1..unfold).joinToString(",") { second })
        }
        val notesMatcher = notes.split(",").map { it.toInt() }
        val recordList = record.map { it }
        val wanted = notesMatcher.sum()
        val cache = mutableSetOf<State12>()
        val count = countCombinationsOpt(cache, State12(notesMatcher, recordList, 0), wanted)
        //println("$record $notes: $count, ${cache.size}")
        count
    }.sum()
}

data class State12(
    val notes: List<Int>,
    val brokenRecord: List<Char>,
    val locked: Int
) {
    var value = -1L
}

fun countCombinationsOpt(cache: MutableSet<State12>, currentState: State12, wanted: Int): Long {
    val (notes, brokenRecord, locked) = currentState
    if (brokenRecord.count { it == '#' || it == '?' } < wanted - locked) {
        return 0
    } else if (brokenRecord.size < notes.sum() + notes.count() - 2) {
        return 0
    }
    if (locked == wanted) {
        return if (notes.isEmpty() && brokenRecord.count { it == '#' } == 0) 1 else 0
    } else if (notes.isEmpty()) {
        return 0
    }
    val next = notes.first()
    val trimmedRecord = brokenRecord.subList(brokenRecord.indexOfFirst { it != '.' }, brokenRecord.size)
    var index = 0
    val maxTrimmed = trimmedRecord.indexOfFirst { it == '#' }.let {
        if (it == -1) trimmedRecord.lastIndex else it
    }
    val combinations = mutableListOf<State12>()
    while (index + next <= trimmedRecord.size && index <= maxTrimmed) {
        val endIndex = index + next
        val testPos = trimmedRecord.subList(index, endIndex)
        if (testPos.all { it != '.' }
            && trimmedRecord.getOrNull(endIndex) != '#') {
            val lockedNext: Int = locked + next
            val startNext = (endIndex + 1).coerceAtMost(trimmedRecord.size)
            combinations.add(
                State12(
                    notes.subList(1, notes.size),
                    trimmedRecord.subList(startNext, trimmedRecord.size),
                    lockedNext
                )
            )
        }
        index++
    }

    var acc = 0L
    val iterator = combinations.toList().chunked(2).iterator()
    if (!iterator.hasNext()) {
        return 0
    }
    do {
        val options = iterator.next()
        val result =
            options.map { option ->
                if (cache.contains(option)) {
                    val optionIndex = cache.indexOf(option)
                    cache.elementAt(optionIndex).value
                } else {
                    val validCombos = countCombinationsOpt(cache, option, wanted)
                    cache.add(option.apply { value = validCombos })
                    validCombos
                }
            }
        acc += result.sum()
    } while (iterator.hasNext() && !(result.all {
            it == 0L
        } && options.first().brokenRecord.all { it != '#' }))
    return acc
}