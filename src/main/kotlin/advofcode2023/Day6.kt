package advofcode2023

fun day6(races: List<Pair<Long, Long>>) = races.map { race ->
    (0..race.first).count {
        it * (race.first - it) > race.second
    }
}.reduceRight { i, acc ->
    i * acc
}
