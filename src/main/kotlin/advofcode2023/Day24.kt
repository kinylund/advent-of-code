package advofcode2023

import Direction
import checkIntersectXY
import checkIntersectXYZ
import com.varabyte.kotter.foundation.collections.liveListOf
import com.varabyte.kotter.foundation.liveVarOf
import com.varabyte.kotter.foundation.render.aside
import com.varabyte.kotter.foundation.session
import com.varabyte.kotter.foundation.text.textLine
import combineAll
import getVectorFromPoints
import partitionN
import pmap
import travel
import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.math.MathContext


fun day24a(dataString: String, limit: ClosedRange<BigDecimal>): Int {
    val hailStones = dataString.split("\n").map {
        val matcherGroupValues = it.split(",", "@").map { it.trim() }
        val (px, py, pz) = matcherGroupValues.subList(0, 3)
        val (vx, vy, vz) = matcherGroupValues.subList(3, 6)
        Triple(
            px.toBigDecimal(MathContext.DECIMAL32),
            py.toBigDecimal(MathContext.DECIMAL32),
            pz.toBigDecimal(MathContext.DECIMAL32)
        ) to Triple(
            vx.toDouble(),
            vy.toDouble(),
            vz.toDouble()
        )
    }

    var intersections = 0
    hailStones.forEachIndexed { index, hailStone ->
        intersections += hailStones.subList(index + 1, hailStones.size).fold(0) { acc, otherHailStone ->
            if (hailStone.checkIntersectXY(otherHailStone, limit)) {
                acc + 1
            } else acc
        }
    }
    return intersections
}

fun day24b(dataString: String, stepSize: Long): Long {
    val hailStones = dataString.split("\n").map {
        val matcherGroupValues = it.split(",", "@").map { it.trim() }
        val (px, py, pz) = matcherGroupValues.subList(0, 3)
        val (vx, vy, vz) = matcherGroupValues.subList(3, 6)
        Triple(
            px.toBigDecimal(MathContext.DECIMAL64),
            py.toBigDecimal(MathContext.DECIMAL64),
            pz.toBigDecimal(MathContext.DECIMAL64)
        ) to Triple(
            vx.toBigDecimal(MathContext.DECIMAL32),
            vy.toBigDecimal(MathContext.DECIMAL32),
            vz.toBigDecimal(MathContext.DECIMAL32)
        )
    }

    val slowestHailstones = hailStones.sortedBy {
        with(it.second) {
            first.abs() + second.abs() + third.abs()
        }
    }.takeLast(20)

    var solution: Triple<BigDecimal, BigDecimal, BigDecimal>? = null
    // Kotter
    session {
        val costString = liveListOf<String>()
        var stepSizeLong by liveVarOf(stepSize)
        section {
            textLine()
            textLine("Searching using step size: $stepSizeLong")
            costString.filter { it.isNotEmpty() }
                .forEachIndexed { index, s ->
                    textLine("Cost was($index): $s")
                }
        }.run {
            aside {
                textLine("Solved so far:")
                textLine()
            }
            var multiplier = 1L
            var searchMultiplier = 4
            (0..2500).map {
                costString.add(it, "")
                it
            }.asIterable()
                .takeWhile { solution == null }
                .pmap { index ->
                    if (solution != null) {
                        return@pmap
                    }
                    val (vectorStones, restList) = slowestHailstones.shuffled().partitionN(2)
                    val limitedSet = restList.take(3)
                    val (s1, s2) = vectorStones
                    val baseScale = 1000L
                    val range = 1L * multiplier..baseScale * multiplier
                    if ((index + 1) % 500 == 0) {
                        multiplier *= 100
                        stepSizeLong = stepSize.toBigInteger().pow(searchMultiplier).toLong()
                        val searchRange =
                            "Searching range  ${1 * multiplier} + ${baseScale * multiplier}, step: $stepSizeLong"
                        aside { textLine(searchRange) }
                        println(searchRange)
                    }
                    val rangeList = listOf(range.random(), range.random()).sorted()
                    val startRange = (rangeList.first()..rangeList.last())
                    val initial = startRange.random() to startRange.random()

                    val goodPoint = solveMin(
                        initial = initial,
                        stepSize.toBigInteger().pow(searchMultiplier).toLong(),
                        updateCost = { cost ->
                            costString[index] = cost.toString()
                        },
                        addResult = { resultString ->
                            aside { textLine(resultString) }
                            //println(resultString)
                        }

                    ) { testParams ->
                        val (t1, t2) = testParams
                        val currVector = createVectorFromParams(s1, t1, s2, t2)

                        val cost = limitedSet.fold(BigDecimal(0, MathContext.DECIMAL128)) { acc, pair ->
                            val timesList = pair
                                .let { it.travel(t1) to it.second }
                                .checkIntersectXYZ(currVector)
                                .toList()
                                .filter { it != ZERO }
                            if (timesList.isEmpty()) {
                                acc
                            } else {
                                val cost = (timesList + timesList.first())
                                    .zipWithNext { a, b ->
                                        a - b
                                    }
                                    .fold(BigDecimal(0, MathContext.DECIMAL128)) { sum, timeDiff ->
                                        timeDiff.pow(2) + sum
                                    }
                                acc + cost
                            }
                        }

                        //println("Cost was: $cost")
                        cost
                    }
                    costString.withWriteLock {
                        costString[index] = ""
                    }
                    goodPoint?.let {
                        val resultVector = createVectorFromParams(s1, it.first, s2, it.second)
                        val startPoint = resultVector.travel(it.first * -1)
                        val solutionString =
                            "Solved with: $it ($s1, $s2, $resultVector, $startPoint)"
                        aside { textLine(solutionString) }
                        println(solutionString)
                        solution = startPoint
                        startPoint
                    }
                }.take(1)
        }
    }
    return solution?.toList()?.sumOf { it.toLong() } ?: 0L
}

private fun createVectorFromParams(
    s1: Pair<Triple<BigDecimal, BigDecimal, BigDecimal>, Triple<BigDecimal, BigDecimal, BigDecimal>>,
    t1: Long,
    s2: Pair<Triple<BigDecimal, BigDecimal, BigDecimal>, Triple<BigDecimal, BigDecimal, BigDecimal>>,
    t2: Long
): Pair<Triple<BigDecimal, BigDecimal, BigDecimal>, Triple<BigDecimal, BigDecimal, BigDecimal>> {
    val s1Pos = s1.travel(t1)
    val s2Pos = s2.travel(t2)
    val time = (t2 - t1).toBigDecimal()
    val currVector = try {
        s1Pos to getVectorFromPoints(s1Pos, s2Pos)
            .let { (vx, vy, vz) ->
                if (time == ZERO) {
                    Triple(vx, vy, vz)
                } else {
                    Triple(
                        vx.divide(time, MathContext.DECIMAL64),
                        vy.divide(time, MathContext.DECIMAL64),
                        vz.divide(time, MathContext.DECIMAL64)
                    )
                }
            }
    } catch (e: ArithmeticException) {
        println(e)
        throw e
    }
    return currVector
}

fun solveMin(
    initial: Pair<Long, Long>,
    stepSize: Long,
    maxIterations: Int = 300,
    tolerance: BigDecimal = 0.00000000001.toBigDecimal(),
    updateCost: (BigDecimal) -> Unit,
    addResult: (String) -> Unit,
    euclideanDistance: (Pair<Long, Long>) -> BigDecimal
): Pair<Long, Long>? {
    var currentPoint = initial
    for (iteration in 1..maxIterations) {
        val (stepSizeX, stepSizeY) = calculateStepSize(stepSize, currentPoint, euclideanDistance) ?: run {
            addResult("Found local optima($iteration): $currentPoint: ${euclideanDistance(currentPoint)}")
            return null
        }
        val nextPoint = Pair(
            currentPoint.first + stepSizeX,
            currentPoint.second + stepSizeY
        )

        val error = euclideanDistance(nextPoint)
        // println("Got $error with $nextPoint")
        updateCost(error)
        if (error < tolerance) {
            val error = euclideanDistance(nextPoint)
            println("Solved in: $iteration with $error: $nextPoint")
            return nextPoint
        }

        currentPoint = nextPoint
    }

    return null
}

fun calculateStepSize(
    alpha: Long,
    currentPoint: Pair<Long, Long>,
    euclideanDistance: (Pair<Long, Long>) -> BigDecimal
): Pair<Long, Long>? {
    val beta = 0.5 // Factor to reduce step size (0 < beta < 1)
    val currentScore = euclideanDistance(currentPoint)
    val searchDirections = getSearchDirections()
        .mapNotNull { searchDirection ->
            generateSequence(alpha) {
                (it * beta).toLong()
            }.takeWhile {
                it != 0L
            }.map { stepSize ->
                val cost = euclideanDistance(
                    Pair(
                        currentPoint.first + stepSize * searchDirection.first,
                        currentPoint.second + stepSize * searchDirection.second
                    )
                )
                (stepSize to cost) to searchDirection
            }.filter {
                it.first.second < currentScore
            }.minByOrNull {
                it.first.second
            }
        }
        .sortedBy {
            it.first.second
        }.toList()

    val searchDirection = searchDirections.firstOrNull()

    return searchDirection?.let {
        Pair(
            it.first.first * it.second.first,
            it.first.first * it.second.second
        )
    }
}

private fun getSearchDirections() = Direction.entries.take(5)
    .combineAll()
    .asSequence()
    .map {
        it.fold(0 to 0) { acc, combine ->
            acc.first + combine.x to acc.second + combine.y
        }
    }.filter { it.first != 0 || it.second != 0 }