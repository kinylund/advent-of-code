package advofcode2023

fun day15a(dataString: String): Int {
    return dataString.split(",").map(::hashString15).sum()
}

private fun hashString15(str: String) = str.map { it.code }.fold(0) { i: Int, acc: Int ->
    ((acc + i) * 17) % 256
}

fun day15b(dataString: String): Int {
    val parseKey = "(\\w+)([-=])(\\d+)?".toRegex()
    val multiMap = (0..255).map { mutableListOf<Pair<String, Int>>() }
    dataString.split(",").forEach { instruction ->
        parseKey.matchEntire(instruction)?.groupValues?.let { (_, label, op, lens) ->
            val hashKey = hashString15(label)
            when (op) {
                "-" -> multiMap[hashKey].apply {
                    indexOfFirst { it.first == label }.takeIf { it >= 0 }?.also {
                        removeAt(it)
                    }
                }

                "=" -> multiMap[hashKey].apply {
                    indexOfFirst { it.first == label }.takeIf { it >= 0 }?.also {
                        this[it] = label to lens.toInt()
                    } ?: add(label to lens.toInt())
                }
            }
        }

    }
    return multiMap.foldIndexed(0) { index, acc, lenses ->
        acc + lenses.foldIndexed(0) { lensIndex, lensStr, i -> lensStr + i.second * (index + 1) * (lensIndex + 1) }
    }
}
