package advofcode2023

import Direction
import createEmptyMutableMap

/**
 * | is a vertical pipe connecting north and south.
 * - is a horizontal pipe connecting east and west.
 * L is a 90-degree bend connecting north and east.
 * J is a 90-degree bend connecting north and west.
 * 7 is a 90-degree bend connecting south and west.
 * F is a 90-degree bend connecting south and east.
 * . is ground; there is no pipe in this tile.
 * S is the starting position of the animal; there is a pipe on this tile,
 *      but your sketch doesn't show what shape the pipe has.
 */
fun day10a(dataString: String): Int {
    val mapLines = dataString.split("\n")
    var (ypos, xpos) = locateStartPos(mapLines)
    var dir = locateValidStartDirection(mapLines, ypos, xpos)
    var steps = 0
    while (dir != Direction.STOP) {
        xpos += dir.x
        ypos += dir.y
        dir = nextStepMapper(mapLines[ypos][xpos], dir)
        steps++
    }
    return steps / 2
}

fun day10b(dataString: String): Int {
    val mapLines = dataString.split("\n")
    val (yPosStart, xPosStart) = locateStartPos(mapLines)
    val dirStart = locateValidStartDirection(mapLines, yPosStart, xPosStart)
    val startTriple = Triple(yPosStart, xPosStart, dirStart)
    var steps = 0
    val trackStrings = createEmptyMutableMap(mapLines.map { it.map { it } }, '.')

    var (yPos, xPos, dir) = startTriple
    while (dir != Direction.STOP) {
        xPos += dir.x
        yPos += dir.y
        dir = nextStepMapper(mapLines[yPos][xPos], dir)
        trackStrings[yPos][xPos] = mapLines[yPos][xPos]
        steps++
    }
    // println(trackStrings.joinToString("\n") { it.joinToString("") })
    val inclusionsMap = try {
        mapToInclusion(trackStrings, startTriple, true)
    } catch (e: IllegalArgumentException) {
        mapToInclusion(trackStrings, startTriple, false)
    }
    // println(inclusionsMap.joinToString("\n") { it.joinToString("") })
    return inclusionsMap.map {
        it.fold(0) { acc, c -> if (c == 'I') acc + 1 else acc }
    }.sum()
}

fun mapToInclusion(
    trackStrings: MutableList<MutableList<Char>>,
    startTriple: Triple<Int, Int, Direction>,
    right: Boolean
): MutableList<MutableList<Char>> {
    var lastDir: Direction? = null
    var (yPos, xPos, dir) = startTriple
    val inclusionMap = createEmptyMutableMap(trackStrings, '.')
    while (dir != Direction.STOP) {
        try {
            var paintDir = dir.rotateSteps(if (right) 1 else -1)
            val paintStop = lastDir?.rotateSteps(2) ?: paintDir.rotateSteps(if (right) 1 else -1)
            while (paintDir != paintStop) {
                paintLine(paintDir, yPos, xPos, trackStrings, inclusionMap)
                paintDir = paintDir.rotateSteps(if (right) 1 else -1)
            }
        } catch (e: IndexOutOfBoundsException) {
            // println(inclusionMap.map { it.joinToString("") }.joinToString("\n"))
            throw IllegalArgumentException("Painting out of bounds")
        }
        xPos += dir.x
        yPos += dir.y
        lastDir = dir
        dir = nextStepMapper(trackStrings[yPos][xPos], dir)
    }
    return inclusionMap
}

private fun paintLine(
    paintDir: Direction,
    yPos: Int,
    xPos: Int,
    trackStrings: List<List<Char>>,
    inclusionMap: MutableList<MutableList<Char>>
): Pair<Int, Int> {
    var painter = Pair(yPos + paintDir.y, xPos + paintDir.x)
    var count = 0
    while (trackStrings[painter.first][painter.second] == '.') {
        inclusionMap[painter.first][painter.second] = 'I'
        painter = Pair(painter.first + paintDir.y, painter.second + paintDir.x)
        count++
    }
    return painter
}

private fun locateStartPos(mapLines: List<String>): Pair<Int, Int> {
    var xPos = -1
    val yPos = mapLines.indexOfFirst { s: String ->
        val xCand = s.indexOfFirst { it == 'S' }
        if (xCand >= 0) {
            xPos = xCand
            true
        } else false
    }
    return Pair(yPos, xPos)
}

private fun locateValidStartDirection(mapLines: List<String>, yPos: Int, xPos: Int): Direction {
    return listOf(Direction.UP, Direction.DOWN, Direction.LEFT, Direction.RIGHT).first {
        try {
            val nextCand = mapLines[yPos + it.y][xPos + it.x]
            when (it) {
                Direction.UP -> nextCand in listOf('|', 'F', '7')
                Direction.DOWN -> nextCand in listOf('|', 'L', 'J')
                Direction.LEFT -> nextCand in listOf('-', 'L', 'F')
                Direction.RIGHT -> nextCand in listOf('-', '7', 'J')
                Direction.STOP -> throw IllegalStateException("Not possible!")
            }
        } catch (e: IndexOutOfBoundsException) {
            false
        }
    }
}

private fun nextStepMapper(step: Char, dir: Direction): Direction {
    return when (step) {
        '|' -> {
            if (dir == Direction.UP) Direction.UP else Direction.DOWN
        }

        '-' -> {
            if (dir == Direction.RIGHT) Direction.RIGHT else Direction.LEFT
        }

        'L' -> {
            if (dir == Direction.DOWN) Direction.RIGHT else Direction.UP
        }

        'J' -> {
            if (dir == Direction.DOWN) Direction.LEFT else Direction.UP
        }

        '7' -> {
            if (dir == Direction.UP) Direction.LEFT else Direction.DOWN
        }

        'F' -> {
            if (dir == Direction.UP) Direction.RIGHT else Direction.DOWN
        }

        'S' -> Direction.STOP
        else -> throw IllegalStateException("Invalid step $step, $dir")
    }
}
