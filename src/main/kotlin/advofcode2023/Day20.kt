package advofcode2023

import findPeriod

var day20bcounter = 0L
val nrCounter = mutableListOf<Pair<String, Long>>()
fun day20b(dataString: String): Long {
    val opMatcher = """([&,%])?(\w+)""".toRegex()
    val engine = buildEngine(dataString, opMatcher)
    val allConj = getStrings(dataString, opMatcher)
    engine.entries.forEach { part ->
        part.value.second.forEach {
            if (it in allConj) {
                engine[it]?.first?.let { it1 ->
                    it1(part.key, false)
                }
            }
        }
    }

    day20bcounter = 0

    do {
        day20bcounter++
    } while (!engine.press(
            listOf(
                OperationFromToValue(
                    "button",
                    "broadcaster",
                    false
                )
            )
        ).third && day20bcounter < 100000 && day20bcounter <= 14279517
    )

    val freqs = nrCounter.groupBy { it.first }.let {
        it.entries.map {
            it.key to it.value.map {
                it.second
            }.findPeriod()
        }
    }.toMap()
    //println("freq: $freqs")

    return freqs.values.filterNotNull().reduce { acc, l -> acc * l }
}

fun day20a(dataString: String): Long {
    val opMatcher = """([&,%])?(\w+)""".toRegex()
    val engine = buildEngine(dataString, opMatcher)
    val allConj = getStrings(dataString, opMatcher)
    engine.entries.forEach { part ->
        part.value.second.forEach {
            if (it in allConj) {
                engine[it]?.first?.let { it1 ->
                    it1(part.key, false)
                }
            }
        }
    }
    val presses = (1..1000).map {
        val result = engine.press(listOf(OperationFromToValue("button", "broadcaster", false)))
        if (result.third) {
            println("Rx triggered at: $it")
        }
        result
    }.reduce { acc, counters ->
        Triple(acc.first + counters.first, acc.second + counters.second, false)
    }

    return presses.first.toLong() * presses.second.toLong()
}

private fun buildEngine(
    dataString: String,
    opMatcher: Regex
) = dataString.split("\n")
    .map { it.split("->") }
    .associate {
        val (_, op, label) = opMatcher.matchEntire(it.first().trim())!!.groupValues
        label to Pair(op.mapToLogic(label, false), it.last().split(",").map { it.trim() })
    }

private fun getStrings(dataString: String, opMatcher: Regex): List<String> {
    val allConj = dataString.split("\n")
        .map { it.split("->") }
        .map {
            val (_, op, label) = opMatcher.matchEntire(it.first().trim())!!.groupValues
            label to op
        }.filter {
            it.second == "&"
        }.map { it.first }
    return allConj
}


typealias OperationFromToValue = Triple<String, String, Boolean>

private fun Map<String, Pair<(String, Boolean) -> Boolean?, List<String>>>.press(ops: List<OperationFromToValue>): Triple<Int, Int, Boolean> {
    var low = 0
    var high = 0
    var operations = ops
    var rxWasTriggered = false
    do {
        operations = operations.mapNotNull { operation ->
            when (operation.third) {
                true -> ++high
                false -> ++low
            }

            if (operation.second == "rx" && !operation.third) {
                rxWasTriggered = true
            }
            this[operation.second]?.let { op ->
                val next = op.first(operation.first, operation.third)
                op.second.map { labelNext ->
                    next?.let {
                        OperationFromToValue(operation.second, labelNext, it)
                    }
                }
            } ?: listOf()
        }.flatten().filterNotNull()
    } while (operations.isNotEmpty())

    return Triple(low, high, rxWasTriggered)
}