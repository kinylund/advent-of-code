package advofcode2023

fun day9a(dataString: String): Long {
    val lines = dataString.split("\n")
    val games = lines.map { line -> line.split(" ").map { it.toInt() } }
    return games.sumOf {
        recursive9a(it)
    }
}

fun day9b(dataString: String): Long {
    val lines = dataString.split("\n")
    val games = lines.map { line -> line.split(" ").map { it.toInt() } }
    return games.sumOf {
        recursive9b(it)
    }
}

fun recursive9a(game: List<Int>): Long {
    val step = game.windowed(2, 1, false).map { (first, last) ->
        last - first
    }
    return if (step.all { it == 0 }) {
        game.last().toLong()
    } else {
        val result = recursive9a(step)
        game.last() + result
    }
}

fun recursive9b(game: List<Int>): Long {
    val step = game.windowed(2, 1, false).map { (first, last) ->
        last - first
    }
    return if (step.all { it == 0 }) {
        game.first().toLong()
    } else {
        val result = recursive9b(step)
        game.first() - result
    }
}
