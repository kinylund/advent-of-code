package advofcode2023

typealias FilterRule = Pair<String, (Int) -> String?>

fun day19a(dataString: String): Long {
    val (rulesStr, dataStr) = dataString.split("\n\n")
    val dataMaps = dataStr.split("\n").map {
        it.trim('}', '{').split(',').map {
            val (key, value) = it.split('=')
            key to value.toInt()
        }.toMap()
    }
    val ruleRegex = """(\w+)([<,>])(\d+):(\w+)""".toRegex()
    val rulesMap = rulesStr.split("\n").map { rulesLine ->
        val label = rulesLine.takeWhile { it != '{' }
        val rulesList = rulesLine.dropWhile { it != '{' }
            .takeWhile { it != '}' }.trim('{', '}')
            .let { rules ->
                rules.split(",").map {
                    if (it.contains(':')) {
                        val match = ruleRegex.matchEntire(it)
                        val (_, key, operator, term, next) = match!!.groupValues
                        FilterRule(key, operator.mapToOp(term.toInt(), next, null))
                    } else {
                        FilterRule("", "noop".mapToOp(0, it, null))
                    }
                }
            }
        label to rulesList
    }.toMap()

    val res = dataMaps.map { dataMap ->
        var next = "in"
        do {
            rulesMap[next]?.firstNotNullOf { filterRule ->
                (filterRule.second(dataMap.getOrDefault(filterRule.first, 0)))?.also {
                    next = it
                }
            }
        } while (next !in listOf("R", "A"))
        dataMap.takeIf { next == "A" }?.values?.sum()
    }
    return res.filterNotNull().sum().toLong()
}

fun day19b(dataString: String): Long {
    val rulesStr = dataString.split("\n\n").first()
    val ruleRegex = """(\w+)([<,>])(\d+):(\w+)""".toRegex()
    val rulesMap = rulesStr.split("\n").map { rulesLine ->
        val label = rulesLine.takeWhile { it != '{' }
        val rulesList = rulesLine.dropWhile { it != '{' }
            .takeWhile { it != '}' }.trim('{', '}')
            .let { rules ->
                rules.split(",").map {
                    if (it.contains(':')) {
                        val match = ruleRegex.matchEntire(it)
                        val (_, key, operator, term, next) = match!!.groupValues
                        FilterRule(key, operator.mapToOp(term.toInt(), next, null))
                    } else {
                        FilterRule("", "noop".mapToOp(0, it, null))
                    }
                }
            }
        label to rulesList
    }.toMap()
    val ranges = mapOf(
        "x" to (1..4000).toList(),
        "m" to (1..4000).toList(),
        "a" to (1..4000).toList(),
        "s" to (1..4000).toList()
    )
    val res = rulesMap.sumAcceptedValues(ranges, "in")
    return res
}

fun Map<String, List<FilterRule>>.sumAcceptedValues(ranges: Map<String, List<Int>>, next: String): Long {
    if (next == "R") {
        return 0
    } else if (next == "A") {
        return ranges.values.fold(1L) { acc, ints ->
            acc * ints.size
        }
    }
    var sum = 0L
    var rangesMap = ranges
    get(next)?.forEach { filterRule ->
        val label = filterRule.first
        val range = rangesMap[label]
        range?.also {
            val (match, rest) = it.partition { num -> filterRule.second(num) != null }
            match.takeIf { it.isNotEmpty() }?.let {
                val matchNext = filterRule.second(match.first())!!
                sum += this.sumAcceptedValues(
                    replaceKey(rangesMap, label, match),
                    matchNext
                )
            }
            rangesMap = replaceKey(rangesMap, label, rest)
        } ?: run {
            sum += this.sumAcceptedValues(rangesMap, filterRule.second(0)!!)
        }
    }
    return sum
}

private fun replaceKey(rangesMap: Map<String, List<Int>>, label: String, match: List<Int>) =
    (rangesMap.entries.filterNot { it.key == label }.map { it.key to it.value }
            + listOf(label to match)).toMap()


fun String.mapToLogic(label: String, initial: Boolean): (String, Boolean) -> Boolean? {
    val label = label
    var state = initial
    val conjState = mutableMapOf<String, Boolean>()
    return when (this) {
        "%" -> { _, inS ->
            if (!inS) {
                !state.also { state = !state }
            } else {
                null
            }
        }

        "&" -> { s, a ->
            conjState[s] = a
            if (label == "nr" && a) {
                nrCounter.add(s to day20bcounter)
            }
            if (label == "nr" && conjState.values.count { it } > 1) {
                println("$day20bcounter: $conjState")
            }
            !conjState.values.all { it }
        }

        "" -> { _, _ -> false }
        else -> {
            throw IllegalArgumentException("Illegal operation :$this")
        }
    }
}