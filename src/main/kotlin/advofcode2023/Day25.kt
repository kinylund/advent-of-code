package advofcode2023

import Node
import breadthFirstSearch
import breadthFirstTraverse
import createAllCliques
import pmap

suspend fun day25a(dataString: String): Int {
    val connectionsStr = dataString.split("\n")
    val connectionLinks = connectionsStr.map {
        it.split(": ", " ")
            .map { it.trim() }
    }.map {
        val first = it.first()
        val rest = it.drop(1)
        rest.map {
            listOf(it, first)
        }
    }.flatten()
    
    val nodesMap = connectionLinks.flatten().toSet().associateWith {
        Node(it)
    }
    nodesMap.values.pmap { node ->
        val connList = connectionLinks.asSequence().filter { node.id in it }.flatten()
            .toSet()
            .minus(node.id)
            .mapNotNull { nodeId ->
                nodesMap[nodeId]
            }
        node.connections = connList.toSet()
        connList.forEach {
            node.edgeHeatMap[it.id] = 0
        }

    }
    nodesMap.values.pmap { it.createAllCliques() }

    val allCliques = nodesMap.values.map { node ->
        node.cliques
    }.flatten().toSet()
    val weakLinks = nodesMap.values.pmap { node ->
        node.connections.mapNotNull { outgoingLink ->
            setOf(node, outgoingLink).takeIf { link ->
                allCliques.none {
                    it.containsAll(link)
                }
            }
        }
    }.flatten().toSet()
    val weightedWeakLinks = weakLinks.map {
        it to breadthFirstSearch(it.first() to it.last(), setOf(it)).first
    }.sortedByDescending { it.second }

    val weakLinksFiltered = weightedWeakLinks.asSequence().map { it.first }
    val superWeak = findBreakingLink(weakLinksFiltered, nodesMap)
    val cuts = superWeak.second + listOf(superWeak.first!!)
    val cut = cuts.first()
    val sub1 = breadthFirstTraverse(cut.first(), false, cuts.toSet())
    val sub2 = breadthFirstTraverse(cut.last(), false, cuts.toSet())
    return sub2.second.size * sub1.second.size
}

private fun findBreakingLink(
    links: Sequence<Set<Node>>,
    nodesMap: Map<String, Node>
): Pair<Set<Node>?, MutableList<Set<Node>>> {
    val linkIterator = links.iterator()
    val cutList = mutableListOf<Set<Node>>()
    var result: Pair<Int, List<Node>>
    do {
        cutList.add(linkIterator.next())
        result = breadthFirstTraverse(cutList.last().first(),false, cutList.toSet())
    } while (result.second.size == nodesMap.size && linkIterator.hasNext())
    val superWeak = if (result.second.size != nodesMap.size) {
        cutList.last()
    } else null
    return superWeak to cutList.apply { remove(superWeak) }
}

private fun findAllStrongLinks(
    breakingCuts: List<Set<Node>>,
    nodesMap: Map<String, Node>
): List<Set<Node>> {
    return breakingCuts.mapNotNull { link ->
        val remainingCuts = breakingCuts.filter { it != link }
        val result = breadthFirstTraverse(breakingCuts.last().first(), false, remainingCuts.toSet())
        if (result.second.size != nodesMap.size) {
            link
        } else null
    }
}