package advofcode2023

import transpose
import kotlin.math.abs

fun day11a(dataString: String): Int {
    val spaceMatrix = dataString.split("\n").map { row -> row.map { it } }
    val expandedSpaceMatrix = getExpandedSpaceMatrix(spaceMatrix, 2)
    val galaxies = expandedSpaceMatrix.mapIndexed { rowIndex, row ->
        row.mapIndexed { colIndex, c ->
            c.takeIf {
                c == '#'
            }?.let {
                Pair(rowIndex, colIndex)
            }
        }
    }.flatten().filterNotNull()
    return galaxies.mapIndexed { index, galaxy: Pair<Int, Int> ->
        galaxies.drop(index + 1).fold(0) { acc: Int, otherGalaxy: Pair<Int, Int> ->
            val dist = abs(galaxy.first - otherGalaxy.first) + abs(galaxy.second - otherGalaxy.second)
            acc + dist
        }
    }.sum()
}

fun day11b(dataString: String, expandBy: Int): Long {
    val spaceMatrix = dataString.split("\n").map { it.map { it } }
    val galaxies = spaceMatrix.mapIndexed { rowIndex, row ->
        row.mapIndexed { colIndex, c ->
            c.takeIf {
                c == '#'
            }?.let {
                Pair(rowIndex.toLong(), colIndex.toLong())
            }
        }
    }.flatten().filterNotNull()
    val maxRows = galaxies.maxBy { it.first }.first
    val maxCols = galaxies.maxBy { it.second }.second
    val expandedGalaxyX = mutableListOf<Pair<Long, Long>>()
    (0L..maxRows).fold(0) { acc, row ->
        if (galaxies.none { it.first == row }) {
            acc + expandBy - 1
        } else {
            val matches = galaxies.filter { it.first == row }
            expandedGalaxyX.addAll(matches.map { (acc + it.first) to it.second })
            acc
        }
    }
    val expandedGalaxyXY = mutableListOf<Pair<Long, Long>>()
    (0L..maxCols).fold(0) { acc, col ->
        if (expandedGalaxyX.none { it.second == col }) {
            acc + expandBy - 1
        } else {
            val matches = expandedGalaxyX.filter { it.second == col }
            expandedGalaxyXY.addAll(matches.map { it.first to (acc + it.second) })
            acc
        }
    }
    return expandedGalaxyXY.mapIndexed { index, galaxy: Pair<Long, Long> ->
        expandedGalaxyXY.drop(index + 1).fold(0) { acc: Long, otherGalaxy: Pair<Long, Long> ->
            val dist = abs(galaxy.first - otherGalaxy.first) + abs(galaxy.second - otherGalaxy.second)
            acc + dist
        }
    }.sum()
}

private fun getExpandedSpaceMatrix(spaceMatrix: List<List<Char>>, expandBy: Int) = spaceMatrix.map { row ->
    expandRow(row, expandBy)
}.flatten().transpose().map { row ->
    expandRow(row, expandBy)
}.flatten().transpose()

private fun expandRow(row: List<Char>, expandBy: Int) = if (row.all { it == '.' }) {
    (0..<expandBy).map { row }
} else {
    listOf(row)
}