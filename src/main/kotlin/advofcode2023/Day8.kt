package advofcode2023

import findLCMOfList
import wordReader

fun day8a(dataString: String): Int {
    val lines = dataString.split("\n")
    val key: String? = wordReader.find(lines.first())?.value
    requireNotNull(key)
    val navMap: Map<String, List<String>> = lines.drop(2).associate { line ->
        val words = wordReader.findAll(line).map { it.value }.toList()
        assert(words.size == 3)
        (words.first() to words.drop(1))
    }

    val counter = countSteps("AAA", key, navMap)

    return counter
}

private fun countSteps(start: String, key: String, navMap: Map<String, List<String>>): Int {
    var next = start
    var counter = 0
    while (next.last() != 'Z') {
        next = when (key[counter % key.length]) {
            'R' -> navMap[next]?.last()!!
            'L' -> navMap[next]?.first()!!
            else -> throw IllegalStateException("Invalid Key")
        }
        counter++
    }
    return counter
}

fun day8b(dataString: String): Long {
    val lines = dataString.split("\n")
    val keyString: String? = wordReader.find(lines.first())?.value
    requireNotNull(keyString)
    val navMap: Map<String, List<String>> = lines.drop(2).associate { line ->
        val words = wordReader.findAll(line).map { it.value }.toList()
        assert(words.size == 3)
        (words.first() to words.drop(1))
    }
    val startKeys = navMap.keys.filter { it.last() == 'A' }
    val numSteps = startKeys.map { countSteps(it, keyString, navMap) }.map { it.toLong() }
    return findLCMOfList(numSteps)
}
