package advofcode2023

import stringsToDigit

fun day1b(vals: String): Int {
    return day1a(stringsToDigit(vals))
}

fun day1a(vals: String) = vals.split("\n").map { line -> line.filter { it.isDigit() } }.map {
    ("${it.first()}${it.last()}").toInt()
}.reduce { acc, i ->
    acc + i
}