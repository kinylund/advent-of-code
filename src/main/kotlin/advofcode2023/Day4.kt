package advofcode2023

fun day4a(testString: String): Int {
    val spaceSplitter = " +".toRegex()
    return testString.split("\n").sumOf { row ->
        val points = row.split(": ", " | ").map { it.trim() }.drop(1).let { numbers ->
            val winSet = numbers.last().split(spaceSplitter).map {
                it.toInt()
            }.toSet()
            val hits = numbers.first().split(spaceSplitter).map {
                it.toInt()
            }.toSet().intersect(winSet).size
            (0..hits).reduceIndexed { index, acc, _ ->
                if (index == 1) 1 else acc * 2
            }
        }
        points
    }
}

fun day4b(games: List<Int>, cutoff: Int = games.size): Int {
    if (games.isEmpty() || cutoff <= 0) {
        return 0
    }
    val rest = games.drop(1)
    val sum: Int = 1 + games.first().let { hits ->
        (0..<hits).sumOf {
            day4b(rest.drop(it), cutoff = 1)
        }
    }

    return sum + day4b(rest, cutoff - 1)
}
